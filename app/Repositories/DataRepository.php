<?php namespace App\Repositories;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Class DataRepository
 *
 * Showing how to build a report repository using the Eloquent ORM method.
 *
 *
 * @package App\Repositories
 */
class DataRepository extends EloquentRepositoryAbstract {

    public function __construct($project_id, $user_id=null, $type=null, $remarks = null)
    {
        /*
        $this->Database = DB::table('lib_brg_cplx')
                          ->leftJoin('data_actionee', function ($join) use ($type) {
                                        $join->on('data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                                             ->where('data_actionee.type', '=', $type);
                                    }
                                );
                          
        if ($user_id) {
            $this->Database->where('data_actionee.users_id', $user_id);
        }
        */
        
        $arrColumn[] = 'lib_brg_cplx.*';
        
        $this->Database = DB::table('lib_brg_cplx')
                                ->leftJoin('data_flow', function ($join) use ($type, $remarks) {
                                    if ($remarks != null) {
                                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                                    } else {
                                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                                        ->where('data_flow.action', '=', $type);
                                    }
                                }
                            );
                            
        
        //$this->Database = DB::table('lib_brg_cplx');
        
        if ($user_id) {
            $this->Database->where('data_flow.users_id', $user_id);
        }
        
        if ($type == 'Approval') {
            $arrColumn[] = 'data_response.revision';
            $arrColumn[] = 'data_response.id as revision_id';
            $this->Database->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
        }
        /*
        switch ($type) {
            
            case 'Unassigned':

            break;
            
            case 'Response':
                //$arrColumn[] = 'data_response.revision';
                //$arrColumn[] = 'data_response.id as revision_id';
                $this->Database->join('data_flow', function ($join) use ($type, $remarks) {
                    if ($remarks != null) {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                    } else {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type);
                    }
                });
                
                $this->Database->groupBy('data_flow.lib_brg_cplx_id');
                //$this->Database->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
            break;
            
            default:
                $this->Database->leftJoin('data_flow', function ($join) use ($type, $remarks) {
                    if ($remarks != null) {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                    } else {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type);
                    }
                });
            break;
        }
        */
        
        $this->Database->where('lib_brg_cplx.project_id', $project_id);

        $this->visibleColumns = $arrColumn;

        $this->orderBy = array(array('ActionNo', 'asc'));
    }
}
