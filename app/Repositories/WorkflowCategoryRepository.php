<?php namespace App\Repositories;
/*
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Illuminate\Database\Eloquent\Model;

class WorkflowCategoryRepository extends EloquentRepositoryAbstract
{
    public function __construct(Model $Model)
    {
        $this->Database = $Model;
    
        $this->visibleColumns = array('*');
    
        $this->orderBy = array(array('category_name', 'asc'));
    }
}
*/

//use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class WorkflowCategoryRepository extends Repository
{
    public function model()
    {
        return 'App\Models\Workflow_Category';
    }

}