<?php namespace App\Repositories;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Data_Flow;

/**
 * Class DataRepository
 *
 * Showing how to build a report repository using the Eloquent ORM method.
 *
 *
 * @package App\Repositories
 */
class DataRepositoryFilter extends EloquentRepositoryAbstract {

    /**
     * TODO: Refactor code
     */
    
    public function __construct($project_id, $user_id=null, $type=null, $remarks = null)
    {
        /*
        $this->Database = DB::table('lib_brg_cplx')
                          ->leftJoin('data_actionee', function ($join) use ($type) {
                                        $join->on('data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                                             ->where('data_actionee.type', '=', $type);
                                    }
                                );
                          
        if ($user_id) {
            $this->Database->where('data_actionee.users_id', $user_id);
        }
        */
        
        $arrColumn[] = 'lib_brg_cplx.*';
        /*
        $this->Database = DB::table('lib_brg_cplx')
                                ->leftJoin('data_flow', function ($join) use ($type, $remarks) {
                                    if ($remarks != null) {
                                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                                    } else {
                                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                                        ->where('data_flow.action', '=', $type);
                                    }
                                }
                            );
                            */
                            
        
        $this->Database = DB::table('lib_brg_cplx');
        
        /*
        if ($user_id) {
            //$this->Database->where('data_flow.users_id', $user_id);
        }
        
        if ($type == 'Approval') {
            $arrColumn[] = 'data_response.revision';
            $arrColumn[] = 'data_response.id as revision_id';
            $this->Database->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
        }
        */
        
        switch ($type) {
            
            case Data_Flow::CASE_CLOSE:
                
                $arrColumn[] = 'data_response.revision';
                $arrColumn[] = 'data_response.id as revision_id';
                
                $this->Database->join('data_flow', function ($join) use ($type, $remarks) {
                    $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                });
                
                $this->Database->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
                $this->orderBy = array(array('ActionNo', 'asc'));
            break;
            
            case 'Unassigned':
                $this->Database->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id', 'left outer');
                $this->Database->whereNull('data_actionee.lib_brg_cplx_id');
                $this->orderBy = array(array('ActionNo', 'asc'));
            break;
            
            case 'Pending_Response':
                //$arrColumn[] = 'data_response.revision';
                $arrColumn[] = 'data_actionee.response_due_date';
                $arrColumn[] = 'users.username as lead_actionee';
                $this->Database->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                $this->Database->where('data_actionee.type', 'lead_actionee');
                
                $this->Database->join('users', 'users.id', '=', 'data_actionee.users_id');
                
                #make sure there is no response in table
                //$this->Database->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id', 'left outer');
                //$this->Database->whereNull('data_response.lib_brg_cplx_id');
                
                if ($user_id) {
                    #make sure there is no response in table
                    $this->Database->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id', 'left outer');
                    $this->Database->whereNull('data_response.lib_brg_cplx_id');
                    
                    //$this->Database->leftJoin('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                    //$this->Database->where('data_response.revision', '1');
                    
                    //$this->Database->raw("JOIN data_response ON data_response.lib_brg_cplx_id=lib_brg_cplx.id
                    //       AND data_response.digital_sign_lead IS NULL");
                    
                    /*
                    $this->Database->join('data_flow', function ($join) use ($type, $remarks, $user_id) {
                        $join->on('data_flow.revision_id', '=', 'data_response.id')
                            ->where('data_flow.action', '!=', Data_Flow::SUBMIT_FOR_APPROVAL)
                            ->where('data_flow.users_id', '=', $user_id);
                    }, 'left outer');
                    */
                       //$this->Database->whereNull('data_flow.id');
                       
                        //$this->Database->raw();
                     /*
                    $this->Database->leftJoin('data_response', function ($join) use ($type, $remarks, $user_id) {
                        $join->on('data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                            ->whereNull('data_response.digital_sign_lead');
                    });
                    */
                   
                    $this->Database->where('data_actionee.users_id', $user_id);
                    
                } else {
                    
                    #make sure there is no response in table
                    $this->Database->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id', 'left outer');
                    $this->Database->whereNull('data_response.lib_brg_cplx_id');
                }
                
                $this->orderBy = array(array('ActionNo', 'asc'));
                
            break;
            
            case 'Pending_For_Approval':
                
                $arrColumn[] = 'data_response.revision';
                $arrColumn[] = 'data_actionee.response_due_date';
                $arrColumn[] = 'users.username as lead_actionee';
                $arrColumn[] = 'data_flow.remarks';
                
                $this->Database->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                
                $this->Database->join('users', 'users.id', '=', 'data_actionee.users_id');
                
                $this->Database->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                $this->Database->whereNotNull('data_response.digital_sign_lead');
                
                #get last action
                $this->Database->join('data_flow', 'data_flow.revision_id', '=', 'data_response.id');
                

                $this->Database->whereIn('data_flow.remarks', [Data_Flow::PENDING_FOR_APPROVAL, Data_Flow::APPROVE_STATUS, Data_Flow::REJECT_STATUS] );
                
                
                if ($user_id) {
                    $this->Database->where('data_actionee.users_id', $user_id);
                }
                
                $this->Database->whereRaw("lib_brg_cplx.id NOT IN (SELECT data_flow.lib_brg_cplx_id FROM data_flow
                                                                            WHERE data_flow.action='Case Close' AND data_flow.lib_brg_cplx_id = lib_brg_cplx.id)");
                
                $this->Database->groupBy('data_response.lib_brg_cplx_id');
                
                $this->orderBy = array(array('data_response.revision', 'desc'));
        
                
            break;
            
            case 'Reject':
                $arrColumn[] = 'data_actionee.response_due_date';
                $arrColumn[] = 'users.username as lead_actionee';
                $arrColumn[] = 'data_flow.remarks';
                
                $this->Database->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                $this->Database->where('data_actionee.type', 'lead_actionee');
                
                $this->Database->join('users', 'users.id', '=', 'data_actionee.users_id');
                
                $this->Database->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                $this->Database->where('data_response.digital_sign_lead', '<>', '');
                $this->Database->groupBy('data_response.lib_brg_cplx_id');
                
                #get last action
                $this->Database->join('data_flow', 'data_flow.revision_id', '=', 'data_response.id');
                $this->Database->where('data_flow.remarks', Data_Flow::REJECT_STATUS);
                
                if ($user_id) {
                    $this->Database->where('data_actionee.users_id', $user_id);
                }
                
                $this->orderBy = array(array('ActionNo', 'asc'));
            break;
            
            case 'Response':
                //$arrColumn[] = 'data_response.revision';
                //$arrColumn[] = 'data_response.id as revision_id';
                $this->Database->join('data_flow', function ($join) use ($type, $remarks) {
                    if ($remarks != null) {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                    } else {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type);
                    }
                });
                
                $this->Database->groupBy('data_flow.lib_brg_cplx_id');
                //$this->Database->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
            break;
            
            default:
                $this->Database->leftJoin('data_flow', function ($join) use ($type, $remarks) {
                    if ($remarks != null) {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type)->where('data_flow.remarks', '=', $remarks);
                    } else {
                        $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', $type);
                    }
                });
            break;
        }
        
        
        $this->Database->where('lib_brg_cplx.project_id', $project_id);

        $this->visibleColumns = $arrColumn;

        //$this->orderBy = array(array('ActionNo', 'asc'));

    }
}
