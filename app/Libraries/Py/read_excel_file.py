#!/usr/bin/python

'''
Install:
$ sudo pip install fuzzywuzzy
$ sudo pip install python-Levenshtein
$ sudo pip install xlrd
'''

import config
import MySQLdb
import MySQLdb.cursors
import time
import string
import re
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from sys import argv
from os.path import basename
import csv, codecs, cStringIO
import xlrd
import json
from fuzzywuzzy import fuzz
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

def get_fieldname(txt):
	arr = txt.split('.');
	fieldname = arr[0]
	if (len(arr) > 1):
		fieldname = arr[1]
	return fieldname
	
def insert_log():
	dt = time.strftime("%Y-%m-%d %H:%M:%S")
	data_input = json.dumps(argv)
	filename = basename(filepath)
	sql = "INSERT INTO lib_fuzzy_status (username, start_datetime, process_status, data_input, filename, project_id) VALUES ('%s', '%s', '%s', '%s', '%s', %d)" % (
	username, dt, 'Starting process...', data_input, filename, int(project_id))
	cursor.execute(sql)

def update_log(done=None):
	if (done is None):
		percents = round(100.00 * (iteration / float(total_rows)), 0)
	else:
		percents = done
	sql = "UPDATE lib_fuzzy_status SET process_status = '%s' WHERE id = %d" % (percents, last_insert_id)
	cursor.execute(sql)

	
def read_file_xlsx():
	workbook = xlrd.open_workbook(read_file)
	worksheet = workbook.sheet_by_index(int(worksheet_index))
	resultsCompareTo = []
	for row_n in range(0, worksheet.nrows):    # Iterate through rows
		cell_value = worksheet.cell(row_n, int(compare_to)).value
		resultsCompareTo.append(cell_value)

	return resultsCompareTo;

script, filepath, username, read_file = argv

print 'Traceback' #enable debug mode for web file v_fuzzy.php

db = MySQLdb.connect(config.MYSQL_HOST,config.MYSQL_USERNAME,config.MYSQL_PASSWORD,config.MYSQL_DB, cursorclass=MySQLdb.cursors.DictCursor)

staging = re.search( r"staging", filepath, re.M|re.I)
if staging:
	db = MySQLdb.connect(config.MYSQL_HOST,config.MYSQL_USERNAME,config.MYSQL_PASSWORD, 'larafive_staging', cursorclass=MySQLdb.cursors.DictCursor)

cursor = db.cursor()

#log to table
insert_log()
last_insert_id = cursor.lastrowid

#create csv file
csvfile  = open(filepath, "wb")
writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
header = ["Provided", "Compare To Column " + compare_to, "Percentage"]
writer.writerow(header);


#Read file
import os.path
extension = os.path.splitext(read_file)[1]

#sys.exit()

iteration = 1;

if extension == '.xlsx':
	workbook = xlrd.open_workbook(read_file)
	worksheet = workbook.sheet_by_index(int(worksheet_index))
	num_cols = worksheet.ncols 
	total_rows = worksheet.nrows
	arrRangePercent = [10, 45, 75, 100]
	n = 150
	while n < total_rows:
		arrRangePercent.append(n)
		n = n + 300

	arrCellValue = []
	for row_n in range(0, worksheet.nrows):

		if iteration in arrRangePercent:
			update_log()
		iteration = iteration + 1

		if worksheet.cell_type(row_n, int(compare_to)) in (xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_BLANK):
			continue

		cell_value_to_compare = worksheet.cell(row_n, int(compare_to)).value

		for row in resultsProvided:
			provided_data = row[provided]
			ratio = fuzz.ratio(str(provided_data), str(cell_value_to_compare))
			if ratio > int(percentage):
				arrCellValue = [provided_data, cell_value_to_compare, ratio]
				for col_idx in range(0, num_cols): 
					cell_value_col = worksheet.cell_value(row_n, col_idx)
					arrCellValue.append(cell_value_col)
				writer.writerow(arrCellValue)


update_log('Process Done')
db.close()
csvfile.close()

msg = MIMEMultipart('alternative')
msg['From'] = config.EMAIL_AMS_FROM
msg['To'] = username + "@aramis-gr.com"
msg['Subject'] = 'Larafive: Load Excel Process Done'
html = """\
	<html>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	  <head>Larafive: Load Excel Process Done</head>
	  <body>
	  <p>Hi!<br>
		The Larafive has successfully process your fuzzy request. Kindly please login to Larafive 
		and download the result.
		</p>
		<p><a href='http://lara5.com/fuzzy' target='_blank'>http://lara5.com/</a></p>
	  </body>
	</html>
	"""
text = html
part1 = MIMEText(text, 'plain')
part2 = MIMEText(html, 'html')
msg.attach(part1)
msg.attach(part2)
mailserver = smtplib.SMTP('mail.aramis-gr.com', 465)
mailserver.ehlo()
mailserver.ehlo()
mailserver.login(msg['From'], config.EMAIL_AMS_FROM_PASSWORD)
mailserver.sendmail(msg['From'], msg['To'], msg.as_string())
mailserver.quit()
