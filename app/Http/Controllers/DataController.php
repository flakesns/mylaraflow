<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
//use App\Http\Controllers;
//use App\Http\Controllers\Controller;
use App\User;
use Validator;
use App\Repositories\AuditRepository as Audit;
use App\Repositories\DataRepository;
use App\Repositories\DataRepositoryFilter;
use App\Models\lib_brg_cplx;
use App\Models\attachments;
use App\Models\Workflow_Category;
use App\Models\Data_Actionee;
use App\Models\Data_Flow;
use App\Models\Data_Response;
use App\Models\TempAction;
use App\Jobs\SendEmailRequestResponse;
use Auth;
use Flash;
use Excel;
use GridEncoder;
use Mail;
use View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use PhpSpec\CodeGenerator\Generator\PrivateConstructorGenerator;
use App\Models\DataColumn;
use PDF;
use DB;
use GuzzleHttp\json_decode;
use App\Models\LibColumnName;
use Illuminate\Contracts\Queue\ShouldQueue;

class DataController extends MyController implements ShouldQueue
{
    public function __construct(){
        parent::__construct();
    }
    
    public function index()
    {
        $homeRouteName = 'listData';

        return \Redirect::route($homeRouteName);
    }

    public function listData()
    {
        $page_title = "List Data";
        $page_description = "Items not sending to actionee...";
        
        #find error data without valid workflow category after submit (from import)
        $datedb = date('Y-m-d');
        $modelError = lib_brg_cplx::where('updated_at', 'like', "$datedb%")->get();

        return view('data.list_data', compact('page_title', 'page_description', 'modelError'));
    }

    public function grid_list_data(Request $request)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->hasRole('coordinator')) {
            $user_id = null;
        }
        
        $type = 'Response';
        $remarks = 'Pending';
        
        GridEncoder::encodeRequestedData(new DataRepository($this->project_id, $user_id, $type, $remarks), $request->all());
    }
    
    public function create()
    {
        $page_title = "Add New Data";
        $page_description = "...";
        
        return view('data.create', compact('page_title', 'page_description'));
    }
    
    public function create_post(Request $request)
    {
        /*
        $validator = Validator::make($request->all(), [
            'ActionNo' => 'required|numeric|digits_between:1,5',
        ]);
    
        if ($validator->fails()) {
            return redirect('lib-data/create')
                ->withInput()
                ->withErrors($validator);
        }
        */
        
        $attributes = $request->all();
        
        $lib = new lib_brg_cplx();
        
        if (!$lib->validate($attributes)) {
            return redirect('lib-data/create')->withInput()->withErrors($lib->error_validator);
        }

        $lib->fill($attributes);
        $lib->project_id = $this->project_id;
        if ( $lib->save() ) {
            Audit::log(Auth::user()->id, 'Data', "Insert new data ActionNo: {$lib->ActionNo}");
            Flash::success('New data successfully created.');
        } else {
            Flash::error("Sorry, failed to save!");
            return redirect('lib-data/create')
            ->withInput()
            ->withErrors($validator);
        }
        
        return redirect('lib-data/');
        
    }
    
    /**
     * TODO: Refactor
     *
     * @param Request $request
     * @param unknown $id
     * @param string $tab
     * @param string $workflow_id
     * @return void
     */
    public function edit(Request $request, $id, $tab = null, $workflow_id = null)
    {
        $model = lib_brg_cplx::find($id);
        
        $page_title = "Edit Data";
        $page_description = "ActionNo# " . $model->ActionNo;
        
        $attachments = lib_brg_cplx::find($id)->attachments;
        
        $workflows = [''=>'[Select Category]']  + Workflow_Category::all()->lists('category_name', 'id')->toArray();
        $users = [''=>'None'] + User::all()->lists('fullname', 'id')->toArray();
        
        if ($model->discipline != '' && !Auth::user()->can('assign-actionee')) {
            $discipline = $model->discipline . "_DISC%";
            $workflows = [''=>'[Select Category]']  + Workflow_Category::where('category_name', 'like', $discipline)->lists('category_name', 'id')->toArray();
        }
        
        $modelWorkflow = Data_Actionee::get_data_actionee($id);
        $response_due_date = $modelWorkflow->response_due_date;
           
        if ($workflow_id != null) {
            $modelWorkflow = Workflow_Category::find($workflow_id);
            $model->workflow_category_id = $workflow_id;
            $modelWorkflow->approval_1_id = $modelWorkflow->approval_1;
            $modelWorkflow->approval_2_id = $modelWorkflow->approval_2;
            $modelWorkflow->approval_3_id = $modelWorkflow->approval_3;
            $modelWorkflow->approval_4_id = $modelWorkflow->approval_4;
            $modelWorkflow->approval_4_id = $modelWorkflow->approval_5;

            if ($response_due_date != '') {
                $modelWorkflow->response_due_date = $response_due_date;
            }
            
        } elseif ($model->workflow_category_id != '' && $modelWorkflow->approval_1 == '') {
            $modelWorkflow = Workflow_Category::find($model->workflow_category_id);
            /*$modelWorkflow->approval_1_id = $modelWorkflow->approval_1;
            $modelWorkflow->approval_2_id = $modelWorkflow->approval_2;
            $modelWorkflow->approval_3_id = $modelWorkflow->approval_3;
            $modelWorkflow->approval_4_id = $modelWorkflow->approval_4;
            $modelWorkflow->approval_4_id = $modelWorkflow->approval_5;*/
            for ($i = 1; $i < 6; $i++) {
                $app_id = "approval_{$i}_id";
                $app_val = "approval_{$i}";
                $modelWorkflow->$app_id = $modelWorkflow->$app_val;
                $modelWorkflow->$app_val = '';
            }
        } else {
        
            $user = User::find($modelWorkflow->lead_actionee);
            if ($user) {
                $modelWorkflow->userLead = $user->fullname;
                $modelWorkflow->lead_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->lead_actionee);
                $user = User::find($modelWorkflow->approval_1);
                $modelWorkflow->approval_1_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_1);
                $modelWorkflow->approval_1_id = $modelWorkflow->approval_1;
                $modelWorkflow->approval_1 = $user->fullname;
                if ($modelWorkflow->approval_2) {
                    $user = User::find($modelWorkflow->approval_2);
                    $modelWorkflow->approval_2_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_2);
                    $modelWorkflow->approval_2_id = $modelWorkflow->approval_2;
                    $modelWorkflow->approval_2 = $user->fullname;
                }
                if ($modelWorkflow->approval_3) {
                    $user = User::find($modelWorkflow->approval_3);
                    $modelWorkflow->approval_3_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_3);
                    $modelWorkflow->approval_3_id = $modelWorkflow->approval_3;
                    $modelWorkflow->approval_3 = $user->fullname;
                }
                if ($modelWorkflow->approval_4) {
                    $user = User::find($modelWorkflow->approval_3);
                    $modelWorkflow->approval_4_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_4);
                    $modelWorkflow->approval_4_id = $modelWorkflow->approval_4;
                    $modelWorkflow->approval_4 = $user->fullname;
                }
                if ($modelWorkflow->approval_5) {
                    $user = User::find($modelWorkflow->approval_5);
                    $modelWorkflow->approval_5_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_5);
                    $modelWorkflow->approval_5_id = $modelWorkflow->approval_5;
                    $modelWorkflow->approval_5 = $user->fullname;
                }
            }
        }
        
        $modelStatus = Data_Flow::where('lib_brg_cplx_id', $id)->orderBy('created_at')->get();

        $arrW = array(
            'users_id'=>Auth::user()->id,
            'lib_brg_cplx_id' => $id
            
        );
        $modelResponse = Data_Response::where($arrW)->orderBy('revision')->get();
        if ($modelResponse->isEmpty()) {
            //$modelResponse = new Data_Response();
            //$modelResponse->revision = 1;
        }
        
        #get data header
        //$model = DataColumn::where('lib_brg_cplx_id', $id)->first();
        $data_cols = $model->data_column()->whereHas('column_name',  function ($query) {
                    $query->where('active', 1);
                })->get();
        foreach ($data_cols as $obj) {
            $arrColumnHeader[$obj->lib_column_name_id] = $obj->column_name->column_name;
        }
        
        //print_r($arrColumnHeader);
        
        return view('data.edit', compact('model', 'arrColumnHeader', 'data_cols', 'attachments', 'workflows', 'users', 'modelWorkflow', 'modelStatus', 'modelResponse', 'tab', 'page_title', 'page_description'));
    }
    
    public function view_data(Request $request, $id, $tab = null, $workflow_id = null)
    {
        $model = lib_brg_cplx::find($id);
        
        $page_title = "Pending Data for Approval";
        $page_description = "ActionNo# " . $model->ActionNo;
        
        $modelWorkflow = Data_Actionee::get_data_actionee($id);
        
        $discipline_roles_id = lib_brg_cplx::get_user_discipline_roles(Auth::user()->id);
        $users = [''=>'None'] + lib_brg_cplx::list_users_by_roles('Approver', $discipline_roles_id); //User::all()->lists('fullname', 'id')->toArray();
        
        $user = User::find($modelWorkflow->lead_actionee);
        
        $modelWorkflow->userLead = $user->fullname;
        $modelWorkflow->lead_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->lead_actionee);
        $user = User::find($modelWorkflow->approval_1);
        $modelWorkflow->approval_1_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_1);
        $modelWorkflow->approval_1_id = $modelWorkflow->approval_1;
        $modelWorkflow->approval_1 = $user->fullname;
        if ($modelWorkflow->approval_2) {
            $user = User::find($modelWorkflow->approval_2);
            $modelWorkflow->approval_2_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_2);
            $modelWorkflow->approval_2_id = $modelWorkflow->approval_2;
            $modelWorkflow->approval_2 = $user->fullname;
        }
        if ($modelWorkflow->approval_3) {
            $user = User::find($modelWorkflow->approval_3);
            $modelWorkflow->approval_3_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_3);
            $modelWorkflow->approval_3_id = $modelWorkflow->approval_3;
            $modelWorkflow->approval_3 = $user->fullname;
        }
        if ($modelWorkflow->approval_4) {
            $user = User::find($modelWorkflow->approval_3);
            $modelWorkflow->approval_4_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_4);
            $modelWorkflow->approval_4_id = $modelWorkflow->approval_4;
            $modelWorkflow->approval_4 = $user->fullname;
        }
        if ($modelWorkflow->approval_5) {
            $user = User::find($modelWorkflow->approval_5);
            $modelWorkflow->approval_5_latest_action = Data_Flow::get_last_action($id, $modelWorkflow->approval_5);
            $modelWorkflow->approval_5_id = $modelWorkflow->approval_5;
            $modelWorkflow->approval_5 = $user->fullname;
        }

        $modelStatus = Data_Flow::where('lib_brg_cplx_id', $id)->orderBy('created_at')->get();
        
        $arrW = array(
            //'users_id'=>Auth::user()->id,
            'lib_brg_cplx_id' => $id
        
        );
        $modelResponse = Data_Response::where($arrW)->orderBy('id', 'desc')->first();
        
        $modelFlow = false;
        $compare_sign_1st = false;
        $compare_sign_2nd = false;
        $compare_sign_3rd = false;
        if ($modelResponse) {
            if ($modelResponse->flow_id_1 != '') {
                $modelFlow = Data_Flow::find($modelResponse->flow_id_1);
                $compare_sign_1st = $this->_compare_digital_sign($id, $modelResponse->id, $modelResponse->digital_sign_approval_1, 'APPROVAL', $modelResponse->flow_id_1);
            }
            if ($modelResponse->flow_id_2 != '') {
                $modelFlow = Data_Flow::find($modelResponse->flow_id_2);
                $compare_sign_2nd = $this->_compare_digital_sign($id, $modelResponse->id, $modelResponse->digital_sign_approval_2, 'APPROVAL', $modelResponse->flow_id_2);
            }
            if ($modelResponse->flow_id_3 != '') {
                $modelFlow = Data_Flow::find($modelResponse->flow_id_3);
                $compare_sign_3rd = $this->_compare_digital_sign($id, $modelResponse->id, $modelResponse->digital_sign_approval_3, 'APPROVAL', $modelResponse->flow_id_3);
            }
            if (!$modelFlow) {
                $modelFlow = new Data_Flow();
                $modelFlow->remarks = Data_Flow::PENDING_FOR_APPROVAL;
            }
            
            $compare_sign_lead = $this->_compare_digital_sign($id, $modelResponse->id, $modelResponse->digital_sign_lead);
            
            $is_can_approve = $modelResponse->is_can_approve($id, $modelResponse->id, Auth::user()->id);
        }
        
        
        
        /*
        $final_approval = $modelResponse->is_final_approval($id, $modelResponse->id);
        if (!$final_approval) {
            $final_approval = new \stdClass();
            $final_approval->remarks = Data_Flow::PENDING_FOR_APPROVAL;
        }
        */
        
        //for revision history
        $arrW = array(
            'lib_brg_cplx_id' => $id
        
        );
        $modelResponseHistory = Data_Response::where($arrW)->orderBy('revision')->get();
        
        $data_cols = $model->data_column()->whereHas('column_name',  function ($query) {
            $query->where('active', 1);
        })->get();
        foreach ($data_cols as $obj) {
            $arrColumnHeader[$obj->lib_column_name_id] = $obj->column_name->column_name;
        }
        
        
        return view('data.view_pending', compact('model', 'data_cols', 'arrColumnHeader', 'modelWorkflow', 'modelStatus', 'modelResponse', 'modelFlow',
                'compare_sign_lead', 'compare_sign_1st', 'compare_sign_2nd', 'compare_sign_3rd', 'is_can_approve', 'modelResponseHistory',
                'tab', 'users', 'page_title', 'page_description'));
    }
    
    public function update(Request $request, $id)
    {
        $attributes = $request->all();
        
        $lib = lib_brg_cplx::findOrFail($request->id);
        
        if (!$lib->validate($attributes)) {
            return redirect("lib-data/edit/{$request->id}")->withInput()->withErrors($lib->error_validator);
        }

        $lib->fill($attributes);
        $lib->save();
        
        Flash::success("Data successfully updated.");
        
        $replayAtt = $attributes;
        $replayAtt["id"] = $id;
        $tmp = Audit::log( Auth::user()->id, 'Data', "Update data ID: {$request->id}",
                            $replayAtt, "App\Http\Controllers\DataController::ParseUpdateAuditLog", "admin.users.replay-edit" );
        
        return redirect("lib-data/edit/{$request->id}");
    }
    
    public function update_actionee(Request $request, $id)
    {
        $attributes = $request->all();
        
        $model = new Data_Actionee();
        
        if (!$model->validate($attributes)) {
            return redirect("lib-data/edit/{$request->id}/actionee/{$request->workflow_category_id}")->withInput()->withErrors($model->error_validator);
        }
        
        $lib = lib_brg_cplx::findOrFail($request->id);
        $lib->workflow_category_id = $request->workflow_category_id;
        $lib->save();
        Audit::log(Auth::user()->id, 'Data', "Assign Workflow: {$lib->ActionNo}");
        
        //$current = Data_Actionee::get_data_actionee($id);
        $deletedRows = Data_Actionee::where('lib_brg_cplx_id', $id)->forceDelete();
        
        $arrValidKey = array('lead_actionee', 'approval_1', 'approval_2', 'approval_3');
        $no_error = true;
        foreach ($attributes as $key=>$value) {
            if ($key == 'lead_actionee') {
                $newModel = new Data_Actionee();
                $newModel->lib_brg_cplx_id = $id;
                $newModel->type = 'lead_actionee';
                $newModel->users_id = $request->lead_actionee;
                $newModel->response_due_date = $request->response_due_date;
                if (!$newModel->save()) {
                    $no_error = false;
                } else {
                    $user = User::find($request->lead_actionee);
                    $lead_name = $user->fullname;
                    Data_Flow::log(Auth::user()->id, $request->id, null, "Assign Actionee", "Assign to $lead_name");
                    Data_Flow::log($request->lead_actionee, $request->id, null, "Response", "Pending");
                }
            } elseif (in_array($key, $arrValidKey) && $value) {
                $newModel = new Data_Actionee();
                $newModel->lib_brg_cplx_id = $id;
                $newModel->type = $key;
                $newModel->users_id = $value;
                $newModel->save();
            }
        }
        
        if ($no_error) {
            if ($this->_sendEmailNotification($request->lead_actionee, $request, 'ASSIGN_LEAD')) {
                Flash::success('Success send mail notification to Lead_Actionee.');
            } else {
                Flash::error('Failed to send mail notification to Lead_Actionee!');
            }
        }
        
        return redirect("lib-data/edit/{$request->id}/status");
    }
    
    public function update_approver(Request $request)
    {
        
    }
    
    private function _sendEmailNotification($user_id, $request, $email_type = null)
    {
        $user = User::findOrFail($user_id);
        $model = lib_brg_cplx::find($request->id);
        $modelResponse = new Data_Response();
        $modelFlow = new Data_Flow();
        if (isset($request->revision_id)) {
            $modelResponse = Data_Response::find($request->revision_id);
        }
        if (isset($request->flow_id)) {
            $modelFlow = Data_Flow::find($request->flow_id);
        }
        $data = array(
            'user'=>$user,
            'model'=> $model,
            'modelResponse'=> $modelResponse,
            'modelFlow' => $modelFlow,
            'request'=>$request
        );
        
        $remarks = "Send to $user->email";
        
        $request->url = url();
                
        if (isset($request->response_due_date)) {
            $request->response_due_date = $request->response_due_date;
        }
        if (isset($request->approval_status)) {
            $request->approval_status = $request->approval_status;
        }
        
        switch ($email_type) {
            case 'TO_APPROVAL_1':
                $data = array(
                    'user'=>$user,
                    'model'=> $model,
                    'modelResponse'=> $modelResponse,
                );
                $file = "emails.request_approval_1";
                $subject = "Requesting Approval for ActionNo: " . $model->ActionNo;
                $remarks .= " Revision $modelResponse->revision";
                
            break;
            
            case Data_Flow::APPROVE_STATUS:
                $file = "emails.approve_status";
                $subject = "ActionNo: " . $model->ActionNo . " Approved!";
                $remarks = false;
            break;
            
            case Data_Flow::REJECT_STATUS:
                $file = "emails.reject_status";
                $subject = "ActionNo: " . $model->ActionNo . " Rejected!";
                $remarks = false;
            break;
            
            case 'NEXT_APPROVER_ACTION':
                $file = "emails.next_approver";
                $subject = "Require your approval for ActionNo: " . $model->ActionNo;
                Data_Flow::log(Auth::user()->id, $request->id, $request->revision_id, Data_Flow::APPROVAL, $remarks);
                Data_Flow::log($user_id, $request->id, $request->revision_id, Data_Flow::APPROVAL, Data_Flow::PENDING_FOR_APPROVAL);
                $remarks = false;
            break;
            
            case 'ASSIGN_LEAD':
                $data = array(
                    'user'=>$user,
                    'model'=> $model,
                    'request'=>$request
                );
                $file = "emails.request_response";
                $subject = "Requesting response for ActionNo: " . $model->ActionNo;
                $remarks = false;
            break;
        }
        
        $mail = Mail::queue($file, $data, function ($message) use ($user, $subject) {
            $message->from('ams@aramis-gr.com', 'LaraFlow');
            $message->to($user->email, $user->name);
            $message->subject($subject);
        });
        
        if ($mail && $remarks) {
            Data_Flow::log_update(Auth::user()->id, $request->id, $request->revision_id, $request->action, $remarks);
        }
        
        return $mail;
    }
    
    public function update_response(Request $request, $id)
    {
        $attributes = $request->all();
    
        $arrW = array(
            'users_id'=>Auth::user()->id,
            'lib_brg_cplx_id' => $id,
            'revision' => $request->revision
        
        );
        $modelResponse = Data_Response::where($arrW)->first();
        if (is_null($modelResponse)) {
            $modelResponse = new Data_Response();
        }
        
        if (!$modelResponse->validate($attributes)) {
            return redirect("lib-data/edit/{$request->id}/response")->withInput()->withErrors($modelResponse->error_validator);
        }
        
        $modelResponse->lib_brg_cplx_id = $id;
        $modelResponse->users_id = Auth::user()->id;
        $modelResponse->response = $request->response;
        $modelResponse->revision = $request->revision;
        if ($modelResponse->save()) {
            $this->upload($request, $modelResponse->id);
            Data_Flow::log(Auth::user()->id, $request->id, $modelResponse->id, "Response", "Revison {$request->revision}");
        }
                
        return redirect("lib-data/edit/{$request->id}/response");
    }
    
    public function upload(Request $request, $id)
    {
        /**
         * TODO: rename file if already exists
         */
        $destination_path = storage_path('uploads');
        $files = $request->file('images');
        $file_count = count($files);
        $uploadcount = 0;
        foreach($files as $file) {
            
            if (!is_object($file)) {
                //Flash::error("Sorry, there is no file attached on the form!");
                return;
            }
        
            $validator = Validator::make(
                [
                    'file' => $file,
                    'extension'  => Str::lower($file->getClientOriginalExtension()),
                ],
                [
                    'file' => 'required|max:100000',
                    'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt,csv'
                ]
            );
        
            if($validator->passes()){
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destination_path, $filename);
                if ($upload_success) {
                    $attach = new attachments();
                    $attach->id_lib_brg = $id;
                    $attach->file_name = $filename;
                    $attach->mime = $file->getClientMimeType();
                    $attach->save();
                    $uploadcount++;
                }
            }
        }
        if($uploadcount == $file_count){
            Audit::log(Auth::user()->id, 'Data', 'Attach File');
            Flash::success("File attached successfully.");
        }
        else {
            return redirect("lib-data/edit/{$request->id}/response")->withInput()->withErrors($validator);
        }
        
        return redirect("lib-data/edit/{$request->id}/response");
    }
    
    public function get_file($filename)
    {
        $file_path = storage_path('uploads') . "/" . $filename;
        return Response::download($file_path);
    }
    
    protected function confirm_submit(Request $request, $id, $revision_id)
    {
        $model = lib_brg_cplx::find($id);
        
        $page_title = "Submit for Approval";
        $page_description = "ActionNo# " . $model->ActionNo;

        $modelResponse = Data_Response::find($revision_id);
        
        return view('data.confirmSubmit', compact('model', 'modelResponse', 'page_title', 'page_description'));
    }
    
    protected function submit_for_approval(Request $request, $id, $revision_id)
    {
        $digital_sign = $this->_generate_digital_sign($id, $revision_id);
                
        if ($digital_sign) {
            $model = Data_Response::find($revision_id);
            $model->digital_sign_lead = $digital_sign;
            if ($model->save()) {
            
                $save = Data_Flow::log(Auth::user()->id, $id, $revision_id, Data_Flow::SUBMIT_FOR_APPROVAL);
                
                $approver1 = Data_Actionee::where(['lib_brg_cplx_id'=>$id, 'type'=>'approval_1'])->first();
                
                $request->revision_id = $revision_id;
                
                if ($this->_sendEmailNotification($approver1->users_id, $request, 'TO_APPROVAL_1')) {
                    Data_Flow::log($approver1->users_id, $id, $revision_id, Data_Flow::APPROVAL, Data_Flow::PENDING_FOR_APPROVAL);
                    Flash::success('Success send mail notification request for Approval.');
                } else {
                    Flash::error('Failed to send mail notification to Approver!');
                }
                
            }
        }
        
        return redirect("lib-data/edit/{$request->id}/response");
    }
    
    /**
     * Generate hash for data
     *
     * @param integer $data_id
     * @param integer $revision_id
     * @return string the hash string
     */
    private function _generate_digital_sign($data_id, $revision_id, $type = null, $flow_id=null)
    {
        $digital_sign = false;
        
        $model = lib_brg_cplx::find($data_id);
        $modelResponse = Data_Response::find($revision_id);
        
        $arr = array(
            $model->id, $model->ActionNo,
            $modelResponse->id, $modelResponse->lib_brg_cplx_id, $modelResponse->users_id, $modelResponse->revision, $modelResponse->response,
        );
        
        if ($type == 'APPROVAL') {
            $modelFlow = Data_Flow::find($flow_id);
            $arrFlow = array($modelFlow->users_id, $modelFlow->comments);
            $arr = array_merge($arr, $arrFlow);
        }
        
        $str = implode(',', $arr);
        
        $digital_sign = md5($str);
        
        return $digital_sign;
    }
    
    private function _compare_digital_sign($data_id, $revision_id, $current_digital_sign, $field='digital_sign_lead')
    {
        if ($this->_generate_digital_sign($data_id, $revision_id) == $current_digital_sign) {
            return true;
        }
        
        return false;
    }
        
    public function pending_for_approval()
    {
        $user_id = Auth::user()->id;
        
        $page_title = "Pending For Approval";
        $page_description = "ActionNo. Submitted For Approval";
        
        $type_grid = "Pending_For_Approval";
        
        return view('data.pending_approval', compact('type_grid', 'page_title', 'page_description'));
    }
    
    public function rejected_lists()
    {
        $user_id = Auth::user()->id;
        
        $page_title = "Rejected ActionNo.";
        $page_description = "Re-issue new revision";
        
        $type_grid = "Reject";
        
        return view('data.pending_approval', compact('type_grid', 'page_title', 'page_description'));
    }
    
    public function grid_pending_for_approval(Request $request, $my_approval=null)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->hasRole('coordinator')) {
            $user_id = null;
        }
    
        $type = 'Approval';
        $remarks = Data_Flow::PENDING_FOR_APPROVAL;
        
        if ($my_approval) {
            $remarks = null;
        }
    
        GridEncoder::encodeRequestedData(new DataRepository($this->project_id, $user_id, $type, $remarks), $request->all());
    }
    
    public function pending_for_response()
    {
        $page_title = "Pending For Response";
        $page_description = "Items waiting response from Actionee";
        $type_grid = 'Pending_Response';
        return view('data.pending_response', compact('page_title', 'page_description', 'type_grid'));
    }
    
    /**
     * TODO: Refactor query to single function
     */
    public function close_case(Request $request)
    {
                
        $page_title = "Close Case";
        $page_description = "...";
        $type_grid = 'Pending_Response';
        
        $query = DB::table('lib_brg_cplx')
                ->select('lib_brg_cplx.id as lib_brg_cplx_id', 'lib_brg_cplx.ActionNo', 'data_flow.updated_at as close_date')
                ->join('data_flow', function ($join) {
                    $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                        ->where('data_flow.action', '=', Data_Flow::CASE_CLOSE)->where('data_flow.remarks', '=', Data_Flow::CASE_CLOSE);
                })
                
                ->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
         $query->where('lib_brg_cplx.project_id', $this->project_id);
         
         $filters = array('lib_brg_cplx.ActionNo'=>'');
         if (isset($request->submitFilter)) {
             $filters_ = $request->filter;
             foreach ($filters_ as $key=>$value) {
                 $key = str_replace('"', '', $key);
                 $query->where($key, "like", "$value%");
                 $filters[$key] = $value;
             }
         }
         
         $query->orderBy('ActionNo');
         
         $model = $query->get();
        
        
        return view('data.close_case', compact('model', 'filters', 'page_title', 'page_description', 'type_grid'));
    }
    
    public function grid_global(Request $request, $type_grid=null)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->hasRole('coordinator')) {
            $user_id = null;
        }

        $remarks = null;
        
        switch ($type_grid) {
            case 'Approval':
                $remarks = Data_Flow::PENDING_FOR_APPROVAL;
            break;
            case 'Pending_Response':
                //$type_grid = 'Response';
                $remarks = 'Pending';
            break;
            case 'Pending_For_Approval':
                
                
                
            break;
            case 'Close_Case':
                $type_grid = Data_Flow::CASE_CLOSE;
                $remarks = Data_Flow::CASE_CLOSE;
            break;
            case 'Unassigned':
                $remarks = null;
            break;
        }
    
        GridEncoder::encodeRequestedData(new DataRepositoryFilter($this->project_id, $user_id, $type_grid, $remarks), $request->all());
    }
    
    public function view_approval()
    {
        $user_id = Auth::user()->id;
        
        $page_title = "My Approval";
        $page_description = "...";
        
        return view('data.pending_approval', compact('page_title', 'page_description'));
    }
    
    public function grid_my_approval(Request $request)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->hasRole('coordinator')) {
            $user_id = null;
        }
    
        $type = 'Approval';
        $remarks = null;//Data_Flow::PENDING_FOR_APPROVAL;
    
        GridEncoder::encodeRequestedData(new DataRepository($this->project_id, $user_id, $type, $remarks), $request->all());
    }
    
    protected function confirm_approval(Request $request, $id)
    {
        $model = lib_brg_cplx::find($id);
        
        $page_title = "Confirm Approval";
        $page_description = "ActionNo# " . $model->ActionNo;
        
        $modelResponse = Data_Response::find($request->revision_id);
        
        return view('data.confirmApproval', compact('model', 'modelResponse', 'request', 'page_title', 'page_description'));
    }
    
    protected function update_approval(Request $request, $id)
    {
        $approval_status = $request->approval_status;
        $approver_number = $request->approver_number;

        $flow_id = Data_Flow::log_update(Auth::user()->id, $request->id, $request->revision_id, Data_Flow::APPROVAL, $approval_status, $request->comments);
        
        $digital_sign = $this->_generate_digital_sign($id, $request->revision_id, 'APPROVAL', $flow_id);
        
        //if ($approval_status == Data_Flow::APPROVE_STATUS) {
            $dsa = "digital_sign_approval_" . $approver_number;
            $flo = "flow_id_" . $approver_number;
            $modelResponse = Data_Response::find($request->revision_id);
            $modelResponse->$dsa = $digital_sign;
            $modelResponse->$flo = $flow_id;
            $modelResponse->save();
        //}
        
        $request->flow_id = $flow_id;
        
        $next_user_id = false;
        if ($approval_status == Data_Flow::APPROVE_STATUS) {
            $next_num = (int)$approver_number + 1;
            $approver_type = "approval_" . $next_num;
            $next_user_id = $this->_get_next_approver($request, $id, $approver_type);
            if ($next_user_id) {
                if ($this->_sendEmailNotification($next_user_id, $request, 'NEXT_APPROVER_ACTION')) {
                    Flash::success('Success send mail notification to next approver.');
                } else {
                    Flash::error('Failed to send mail notification to next approver!');
                }
            }
        }
        
        
        if ($next_user_id == false) {
            if ($approval_status == Data_Flow::APPROVE_STATUS) {
                $modelData = lib_brg_cplx::find($id);
                $modelData->case_closed = 1;
                $modelData->save();
                Data_Flow::log(1, $request->id, $request->revision_id, Data_Flow::CASE_CLOSE, Data_Flow::CASE_CLOSE);
            }
            
            if ($this->_sendEmailNotification($modelResponse->users_id, $request, $request->approval_status)) {
                Flash::success('Success send mail notification to Actionee.');
            } else {
                Flash::error('Failed to send mail notification to Actionee!');
            }
        }
        
        return redirect("lib-data/view_pending/{$request->id}");
    }
    
    private function _get_next_approver(Request $request, $id, $type)
    {
        $arr = array('lib_brg_cplx_id'=>$id, 'type'=>$type);
        $model = Data_Actionee::where($arr)->first();
        if (!$model) {
            return false;
        }
        
        return $model->users_id;
    }
    
    public function report_status()
    {
        $page_title = "Action Status Report";
        $page_description = "...";
        
        return view('data.report_status', compact('page_title', 'page_description'));
    }
    
    public function report_close_case()
    {
        $page_title = "Close Case Report";
        $page_description = "...";
        
        return view('data.report_close_case', compact('page_title', 'page_description'));
    }
    
    public function print_data(Request $request, $id)
    {
        $model = lib_brg_cplx::find($id);
        return view('data.print_data', compact('model'));
    }
    
    protected function sendRequestResponse(Request $request, $user_id, $data)
    {
        $user = User::findOrFail($user_id);
        //$data['user'] = $user;
        
        $r = new Request();
        $r->response_due_date = '15/08/2016';
        $r->url = url();
        //$m = lib_brg_cplx::find(15);
        //echo $m->project->project_name; exit;
        $model = lib_brg_cplx::find(15);
        $data = array (
            'user' => $user,
            'model' => $model,
            'request' => $request
        );

        //$this->dispatch(new SendEmailRequestResponse($user, $data));
        
        $subject = "test";
        
       // $model = $data['model'];
        //$request = $data['request'];
        
        Mail::queue('emails.request_response', ['user'=>$user, 'model'=>$model, 'request'=>$r], function ($message) use ($user, $subject) {
            $message->from('ams@aramis-gr.com', 'LaraFlow');
            $message->to($user->email, $user->name);
            $message->subject($subject);
        });
        echo "queeee";exit;
    }
    
    protected function excel_review_temp(Request $request)
    {
        $user_id = Auth::user()->id;
        
        $modelTemp = new TempAction();
        
        if (isset($request->cancel)) {
            $model = TempAction::where(['user_id'=>$user_id, 'project_id'=>$this->project_id])->delete();
            return redirect('import/excel');
        }
        
        if (isset($request->save_only)) {
            $model = $modelTemp->save_only($this->project_id, $user_id);
            Flash::success('Data successfully created.');
            return redirect('lib-data/list-data');
        }
        
        if (isset($request->save_assign)) {
            $model = $modelTemp->save_only($this->project_id, $user_id, true);
                        
            //$lead_id = 18;
            //$data_id = 10;
            //$this->sendRequestResponse($request, $lead_id, $data_id);
            
            Flash::success('Data successfully created & send notification to lead/actionee.');
            
            return redirect('lib-data/pending-for-response');
        }
        
        $page_title = "Review Imported Data";
        $page_description = "...";
     
        #get header
        $model = TempAction::where(['user_id'=>$user_id, 'project_id'=>$this->project_id])->first();
        foreach ($model->temp_data as $obj) {
            $arrColumnHeader[$obj->lib_column_name_id] = $obj->column_name->column_name;
        }
        
        $model = TempAction::where(['user_id'=>$user_id, 'project_id'=>$this->project_id])->get();
        
        return view('data.review_temp', compact('model', 'arrColumnHeader', 'page_title', 'page_description'));
    }
    
    public function print_close_case_single(Request $request, $id, $attach = 'none', $type=null)
    {
        print_r($request->ids);
        
        $str = json_decode($request->ids, true);
        var_dump($str);
        
        exit;
        $destination_path = storage_path('uploads') . "/";
       
        $model = lib_brg_cplx::get_model_close_case($this->project_id, $id, $type);
        
        $arrVisibleColumn = lib_brg_cplx::get_visible_column();
        
        # get the column header
        foreach ($model as $obj) {
            $group_header_id = $obj->group_import_id;
            $group_header_column = json_decode($obj->column_header, true);
        }
        
        foreach ($group_header_column as $arr) {
            $col_id = $arr['col_id'];
            if (isset($arrVisibleColumn[$col_id])) {
                $arrColumnHeader[$col_id] = $arr['col_name'];
            }
        }
        
        foreach ($model as $obj) {
            $lib_brg_cplx_id = $obj->lib_brg_cplx_id;
        
            #get the native data
            $modelData = DataColumn::where('lib_brg_cplx_id', $lib_brg_cplx_id)->get();
            foreach ($modelData as $dat) {
                if (isset($arrVisibleColumn[$dat->lib_column_name_id])) {
                    $col_name = $arrVisibleColumn[$dat->lib_column_name_id];
                    $arr[$col_name] = $dat->data_value;
                }
            }
            
            #get predefine fields
            $arrPre = lib_brg_cplx::get_predefine_action_details($arr, 'Print');
            
            #get mapping column
            $arr = lib_brg_cplx::get_mapping_column($arr);
             
            $arr['close_date'] = ($obj->action == 'Case Close') ? date('d-M-Y', strtotime($obj->close_date)) : '';
            $arr['close_by'] = ($obj->action == 'Case Close') ? lib_brg_cplx::get_user_info($obj->flow_users_id, 'fullname') : '';
            
            #get the response
            $response_id = $obj->response_id;
            $arr['response_user'] = lib_brg_cplx::get_user_info($obj->response_user_id, 'fullname');
            $arr['response_text'] = $obj->response;
            $arr['response_rev'] = $obj->revision;
            
            #get the last response
            $response_rev_id = $obj->response_rev_id;
            $modelResp = Data_Response::where('lib_brg_cplx_id', $lib_brg_cplx_id)->orderBy('id','desc')->first();
            $response_id = $modelResp->id;
            $arr['response_user'] = lib_brg_cplx::get_user_info($modelResp->users_id, 'fullname');
            $arr['response_text'] = $modelResp->response;
            $arr['response_rev'] = $modelResp->revision;
            
            #get rejected flow
            $modelReject = Data_Flow::where('revision_id', $response_id)->where('remarks', Data_Flow::REJECT_STATUS)->get();
            foreach ($modelReject as $rej) {
                $arr['reject_comment'] = $rej->comments;
                $arr['reject_user'] = lib_brg_cplx::get_user_info($rej->users_id, 'fullname');
            }
            
            #get approver flow
            $modelApprove = Data_Flow::where('revision_id', $response_id)
                                        ->where('remarks', Data_Flow::APPROVE_STATUS)
                                        ->orderBy('id')
                                        ->get();
            foreach ($modelApprove as $approve) {
                $arrApprove[$approve->id]['approver_comment'] = $approve->comments;
                $arrApprove[$approve->id]['approver_user'] = lib_brg_cplx::get_user_info($approve->users_id, 'fullname');
                $arrApprove[$approve->id]['approver_date'] = date('d-M-Y', strtotime($approve->updated_at));
                #last approver close the case
                $arr['close_by'] = $arrApprove[$approve->id]['approver_user'];
            }
        }
        
        $pdf = PDF::loadView('data.print_close_case', compact('model', 'modelReject', 'arr', 'arrPre', 'arrApprove', 'arrColumnHeader', 'modelResponse'));

        $fileData[] = $destination_path . 'close_case' . date('YmdHis') . '.pdf';
        $pdf->save($fileData[0]);
        
        if ($attach != 'attach') {
            $headers = [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; ' . $fileData[0],
            ];
            
            return \Response::make(File::get($fileData[0]), 200, $headers);
        }

        //for revision history
        $arrW = array(
            'lib_brg_cplx_id' => $id
        );
        $modelResponse = Data_Response::where($arrW)->orderBy('revision')->first();
        foreach($modelResponse->attachments as $attach) {
            $fileData[] = $destination_path . $attach->file_name;
        }
        
        $mergeFile = $destination_path . "merged_". date('YmdHis') .".pdf";
    
        $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$mergeFile ";
        //Add each pdf file to the end of the command
        foreach($fileData as $file) {
            $cmd .= "'$file'" . " ";
        }
        //echo $cmd . "<br>";
        $result = exec($cmd, $output);
        //print_r($output);
        //echo $outputName;
        //$pdf = PDF::loadFile($mergeFile);
        //return $pdf->stream($mergeFile);
        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; ' . $mergeFile,
        ];
        
        return \Response::make(File::get($mergeFile), 200, $headers);
    }
    
    public function print_close_case(Request $request)
    {
        $ids = json_decode($request->ids, true);
        $attach = $request->attach;
        
        $destination_path = storage_path('uploads') . "/";
         
        $model = lib_brg_cplx::get_model_close_case($this->project_id, $ids);
    
        $arrVisibleColumn = lib_brg_cplx::get_visible_column();
    
        # get the column header
        foreach ($model as $obj) {
            $group_header_id = $obj->group_import_id;
            $group_header_column = json_decode($obj->column_header, true);
        }
    
        foreach ($group_header_column as $arr) {
            $col_id = $arr['col_id'];
            if (isset($arrVisibleColumn[$col_id])) {
                $arrColumnHeader[$col_id] = $arr['col_name'];
            }
        }
    
        foreach ($model as $obj) {
            $lib_brg_cplx_id = $obj->lib_brg_cplx_id;
            $arrResponseId[] = $obj->response_rev_id;
    
            #get the native data
            $modelData = DataColumn::where('lib_brg_cplx_id', $lib_brg_cplx_id)->get();
            foreach ($modelData as $dat) {
                if (isset($arrVisibleColumn[$dat->lib_column_name_id])) {
                    $col_name = $arrVisibleColumn[$dat->lib_column_name_id];
                    $arr[$col_name] = $dat->data_value;
                }
            }
    
            #get predefine fields
            $arrPre = lib_brg_cplx::get_predefine_action_details($arr, 'Print');
    
            #get mapping column
            $arr = lib_brg_cplx::get_mapping_column($arr);
             
            $arr['close_date'] = ($obj->action == 'Case Close') ? date('d-M-Y', strtotime($obj->close_date)) : '';
            $arr['close_by'] = ($obj->action == 'Case Close') ? lib_brg_cplx::get_user_info($obj->flow_users_id, 'fullname') : '';
    
            #get the response
            $response_id = $obj->response_id;
            $arr['response_user'] = lib_brg_cplx::get_user_info($obj->response_user_id, 'fullname');
            $arr['response_text'] = $obj->response;
            $arr['response_rev'] = $obj->revision;
    
            #get the last response
            $response_rev_id = $obj->response_rev_id;
            $modelResp = Data_Response::where('lib_brg_cplx_id', $lib_brg_cplx_id)->orderBy('id','desc')->first();
            $response_id = $modelResp->id;
            $arr['response_user'] = lib_brg_cplx::get_user_info($modelResp->users_id, 'fullname');
            $arr['response_text'] = $modelResp->response;
            $arr['response_rev'] = $modelResp->revision;
    
            #get rejected flow
            $modelReject = Data_Flow::where('revision_id', $response_id)->where('remarks', Data_Flow::REJECT_STATUS)->get();
            foreach ($modelReject as $rej) {
                $arr['reject_comment'] = $rej->comments;
                $arr['reject_user'] = lib_brg_cplx::get_user_info($rej->users_id, 'fullname');
            }
    
            #get approver flow
            $arrApprove = array();
            $modelApprove = Data_Flow::where('revision_id', $response_id)
            ->where('remarks', Data_Flow::APPROVE_STATUS)
            ->orderBy('id')
            ->get();
            foreach ($modelApprove as $approve) {
                $arrApprove[$approve->id]['approver_comment'] = $approve->comments;
                $arrApprove[$approve->id]['approver_user'] = lib_brg_cplx::get_user_info($approve->users_id, 'fullname');
                $arrApprove[$approve->id]['approver_date'] = date('d-M-Y', strtotime($approve->updated_at));
                #last approver close the case
                $arr['close_by'] = $arrApprove[$approve->id]['approver_user'];
            }
            
            $pdf = PDF::loadView('data.print_close_case', compact('model', 'modelReject', 'arr', 'arrPre', 'arrApprove', 'arrColumnHeader', 'modelResponse'));

            $fileData[$lib_brg_cplx_id] = $destination_path . 'close_case' . date('YmdHis') . '.pdf';
            $pdf->save($fileData[$lib_brg_cplx_id]);

        }//close foreach ($model as $obj)
    
        //$pdf = PDF::loadView('data.print_close_case', compact('model', 'modelReject', 'arr', 'arrPre', 'arrApprove', 'arrColumnHeader', 'modelResponse'));
    
        $mergeFile = $destination_path . "merged_". date('YmdHis') .".pdf";
        $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$mergeFile ";
        
        if ($attach != 'attach') {
            
            foreach ($fileData as $file) {
                $cmd .= "'$file'" . " ";
            }
            
            $result = exec($cmd, $output);
            
            $headers = [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; ' . $mergeFile,
            ];
    
            return \Response::make(File::get($mergeFile), 200, $headers);
            
        }//close if ($attach != 'attach')
        
        ###### IF PRINT WITH ATTACHMENT ################################################

        #Get the valid attachment files by revision //$arrResponseId
        $modelResponse = Data_Response::whereIn('id', $arrResponseId)->orderBy('revision')->with('attachments')->get();
        foreach ($modelResponse as $obj) {
            $files_att = array();
            foreach ($obj->attachments as $attach) {
                $files_att[] = $destination_path . $attach->file_name;
            }
            $arrAttach[$obj->lib_brg_cplx_id] = $files_att;
        }
        
        foreach ($fileData as $lib_brg_cplx_id=>$file) {
            $cmd .= "'$file'" . " ";
            foreach ($arrAttach[$lib_brg_cplx_id] as $fileAttach) {
                $cmd .= "'$fileAttach'" . " ";
            }
        }
        
        $result = exec($cmd, $output);
        
        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; ' . $mergeFile,
        ];
        
        return \Response::make(File::get($mergeFile), 200, $headers);
    }
    
    public function export_excel(Request $request, $ids = null)
    {
        if ($ids == null) {
            $model = lib_brg_cplx::get_model_close_case($this->project_id);
        }
        
        Excel::create('close_case', function($excel) use($model) {
        
            $excel->sheet('Close Case', function($sheet) use($model) {
                
                #load visible column
                $arrVisibleColumn = lib_brg_cplx::get_visible_column();
                                
                # get the column header
                foreach ($model as $obj) {
                    $group_header_id = $obj->group_import_id;
                    $group_header_column = json_decode($obj->column_header, true);
                    $arrGroupHeader[$group_header_id] = $group_header_column;
                }
                
                foreach ($arrGroupHeader as $group_header_id => $group_header_column) {
                    foreach ($group_header_column as $arr) {
                        $col_id = $arr['col_id'];
                        if (isset($arrVisibleColumn[$col_id])) {
                            $arrColumnHeader[$group_header_id][$col_id] = $arr['col_name'];
                        }
                    }
                }
                
                $arrAddOn = array('Close Date', 'Lead/Actionee', 'Response', 'Response Rev.', 'Attachment');

                /*
                foreach ($group_header_column as $arr) {
                    $col_id = $arr['col_id'];
                    if (isset($arrVisibleColumn[$col_id])) {
                        $arrColumnHeader[$col_id] = $arr['col_name'];
                    }
                }
                */

                /*
                $arrAddOn = array('Close Date', 'Lead/Actionee', 'Response', 'Response Rev.', 'Attachment');
                $arrColumnHeader = array_merge($arrColumnHeader, $arrAddOn);
                
                $sheet->rows(array($arrColumnHeader));
                
                $cell_n_attachment = array_search('Attachment', $arrColumnHeader);
                */
        
                # get the data
                $row_n = 1;
                $arrLink = null;
                
                $current_group_id = null;
                
                foreach ($model as $obj) {
                    
                    #modify header
                    if ($obj->group_import_id != $current_group_id) {
                        $current_group_id = $obj->group_import_id;
                        $arrNewColumn = $arrColumnHeader[$current_group_id];
                        $arrNewColumnWrite = array_merge($arrNewColumn, $arrAddOn);
                        $sheet->rows(array($arrNewColumnWrite));
                        $cell_n_attachment = array_search('Attachment', $arrNewColumnWrite);
                        //echo "<br>".$cell_n_attachment;
                        $row_n = $row_n + 1;
                    }
                    
                    
                    $arr = array();
                    $lib_brg_cplx_id = $obj->lib_brg_cplx_id;
                    
                    #get the native data
                    $modelData = DataColumn::where('lib_brg_cplx_id', $lib_brg_cplx_id)->get();
                    foreach ($modelData as $dat) {
                        if (isset($arrVisibleColumn[$dat->lib_column_name_id])) {
                            //$arr[] = $dat->data_value;
                            
                            $col_id = $dat->lib_column_name_id;
                            $arrValue[$col_id] = $dat->data_value;
                        }
                    }
                    
                    //print_r($arrNewColumn);
                    #set data to follow column index
                    foreach ($arrNewColumn as $col_id=>$header_name) {
                        $arr[] = $arrValue[$col_id];
                    }
                    
                    $arr[] = date('d-M-Y', strtotime($obj->close_date));
                    
                    #get the response
                    $response_id = $obj->response_id;
                    $arr[] = $obj->response_user_id;
                    $arr[] = $obj->response;
                    $arr[] = $obj->revision;
                    
                    
                    #get the attachment - should be in the last column
                    $i = 1;
                    $modelAttach = attachments::where('id_lib_brg', $response_id)->get();
                    foreach ($modelAttach as $dat) {
                        $arr[] = "Attachment " . $i++;
                        $arrLink[$row_n][] = $dat->file_name;
                    }
                    
                    $row_n++;
                    
                    $sheet->rows(array($arr));
                    
                    if (is_array($arrLink)) {
                        $n = 0;
                        for ($i = 'A'; $i !== 'GZ'; $i++){
                            if ($n == $cell_n_attachment) {
                                $cellStart = $i;
                                break;
                            }
                            $n++;
                        }
                    
                        foreach ($arrLink as $row_numb => $arr) {
                            $cellLink = $cellStart;
                    
                            foreach ($arr as $file_name) {
                                $cellLoc = $cellLink . $row_numb;
                                //echo "<br>$cellLoc";
                                $link = url() . "/files/" . $file_name;
                                $sheet->getCell($cellLoc)->getHyperlink()->setUrl(strip_tags($link));
                                $cellLink++;
                            }
                        }
                    }
                    
                    $arrLink = array();
                    
                }
                
                //$sheet->rows(array($arr));
                
                #generate link for attachment
                /*
                if (is_array($arrLink)) {
                    $n = 0;
                    for ($i = 'A'; $i !== 'GZ'; $i++){
                        if ($n == $cell_n_attachment) {
                            $cellStart = $i;
                            break;
                        }
                        $n++;
                    }

                    foreach ($arrLink as $row_n => $arr) {
                        $cellLink = $cellStart;
                        
                        foreach ($arr as $file_name) {
                            $cellLoc = $cellLink . $row_n;
                            $link = url() . "/files/" . $file_name;
                            $sheet->getCell($cellLoc)->getHyperlink()->setUrl(strip_tags($link));
                            $cellLink++;
                        }
                    }
                }
                */
        
            });
        })
        ->download('xlsx');
        
    }
    
    /**
     public function export_excel(Request $request, $ids = null)
    {
        if ($ids == null) {
            $model = lib_brg_cplx::get_model_close_case($this->project_id);
        }
        
        Excel::create('close_case', function($excel) use($model) {
        
            $excel->sheet('Close Case', function($sheet) use($model) {
                
                #load visible column
                $arrVisibleColumn = lib_brg_cplx::get_visible_column();
                                
                # get the column header
                foreach ($model as $obj) {
                    $group_header_id = $obj->group_import_id;
                    $group_header_column = json_decode($obj->column_header, true);
                }
                                
                foreach ($group_header_column as $arr) {
                    $col_id = $arr['col_id'];
                    if (isset($arrVisibleColumn[$col_id])) {
                        $arrColumnHeader[$col_id] = $arr['col_name'];
                    }
                }
                                
                $arrAddOn = array('Close Date', 'Lead/Actionee', 'Response', 'Response Rev.', 'Attachment');
                $arrColumnHeader = array_merge($arrColumnHeader, $arrAddOn);
                
                $sheet->rows(array($arrColumnHeader));
                
                $cell_n_attachment = array_search('Attachment', $arrColumnHeader);
        
                # get the data
                $row_n = 2;
                $arrLink = null;
                
                foreach ($model as $obj) {
                    $arr = array();
                    $lib_brg_cplx_id = $obj->lib_brg_cplx_id;
                    
                    #get the native data
                    $modelData = DataColumn::where('lib_brg_cplx_id', $lib_brg_cplx_id)->get();
                    foreach ($modelData as $dat) {
                        if (isset($arrVisibleColumn[$dat->lib_column_name_id])) {
                            $arr[] = $dat->data_value;
                        }
                    }
                    
                    $arr[] = date('d-M-Y', strtotime($obj->close_date));
                    
                    #get the response
                    $response_id = $obj->response_id;
                    $arr[] = $obj->response_user_id;
                    $arr[] = $obj->response;
                    $arr[] = $obj->revision;
                    
                    
                    #get the attachment - should be in the last column
                    $i = 1;
                    $modelAttach = attachments::where('id_lib_brg', $response_id)->get();
                    foreach ($modelAttach as $dat) {
                        //$arr[] = $dat->file_name;
                        //$link = url() . "/files/" . $dat->file_name;
                        //$sheet->getCell($cellLink)->getHyperlink()->setUrl(strip_tags($link));
                        //$arr[] = '=HYPERLINK("'.$link.'", "Click for report")';
                        $arr[] = "Attachment " . $i++;
                        $arrLink[$row_n][] = $dat->file_name;
                    }
                    
                    $row_n++;
                    
                    $sheet->rows(array($arr));
                }
                
                //$sheet->rows(array($arr));
                
                #generate link for attachment
                if (is_array($arrLink)) {
                    $n = 0;
                    for ($i = 'A'; $i !== 'GZ'; $i++){
                        if ($n == $cell_n_attachment) {
                            $cellStart = $i;
                            break;
                        }
                        $n++;
                    }

                    foreach ($arrLink as $row_n => $arr) {
                        $cellLink = $cellStart;
                        
                        foreach ($arr as $file_name) {
                            $cellLoc = $cellLink . $row_n;
                            $link = url() . "/files/" . $file_name;
                            $sheet->getCell($cellLoc)->getHyperlink()->setUrl(strip_tags($link));
                            $cellLink++;
                        }
                    }
                }
        
            });
        })
        ->download('xlsx');
        
    }
     */
}
