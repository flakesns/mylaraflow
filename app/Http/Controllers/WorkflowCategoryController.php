<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Repositories\AuditRepository as Audit;
use App\Repositories\WorkflowCategoryRepository as WF_CAT;
use App\Models\Workflow_Category;
use App\User;
use Auth;
use Flash;
use GridEncoder;
//use App\Repositories\App\Repositories;
//use App\Models\App\Models;

class WorkflowCategoryController extends MyController
{
    private $_model;
    
    public function __construct(WF_CAT $model) {
    
        $this->_model = $model;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = "Workflow Category";
        $page_description = "...";

        $categories = $this->_model->paginate(20);
        
        return view('admin.workflow.index', compact('categories', 'page_title', 'page_description'));
    }
    
    public function grid_list_data(Request $request)
    {
        GridEncoder::encodeRequestedData(new WorkflowCategoryRepository(new Workflow_Category()), $request->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Add New Workflow Category";
        $page_description = "...";

        $users = [''=>'None'] + User::all()->lists('fullname', 'id')->toArray();;
        
        return view('admin.workflow.create', compact('users', 'page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        
        $model = new Workflow_Category();
        
        if (!$model->validate($attributes)) {
            return redirect('workflow/create')->withInput()->withErrors($model->error_validator);
        }

        $model->fill($attributes);
        if ( $model->save() ) {
            Audit::log(Auth::user()->id, 'Workflow_Category', "Insert new: {$model->category_name}");
            Flash::success('New data successfully created.');
        } else {
            Flash::error("Sorry, failed to save!");
            return redirect('workflow/create')
            ->withInput()
            ->withErrors($validator);
        }
        
        return redirect('workflow/index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Data";
        $page_description = "...";
        $users = [''=>'None'] + User::all()->lists('fullname', 'id')->toArray();
        $model = $this->_model->find($id);
        
        return view('admin.workflow.edit', compact('model', 'users', 'page_title', 'page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $request->all();
        
        $model = Workflow_Category::findOrFail($request->id);
        
        if (!$model->validate($attributes)) {
            return redirect("workflow/edit/{$request->id}")->withInput()->withErrors($model->error_validator);
        }

        $model->fill($attributes);
        if (!$model->save()) {
            Flash::error('Failed to update!');
            return redirect("workflow/edit/{$request->id}")->withInput();
        }
        
        Flash::success("Data successfully updated.");
        
        $tmp = Audit::log( Auth::user()->id, 'Workflow_Category', "Update data: {$model->category_name}" );
        
        return redirect("workflow/index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
