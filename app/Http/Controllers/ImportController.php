<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Controllers;
use App\User;
use Validator;
use App\Repositories\AuditRepository as Audit;
use App\Models\LibColumnName;
use App\Models\DataColumn;
use App\Models\lib_brg_cplx;
use Auth;
use Flash;
use Excel;

class ImportController extends MyController
{
    private $_arr_column_id = null;
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $homeRouteName = 'excel';

        try {
            $homeCandidateName = config('app.home_route');
            $homeRouteName = $homeCandidateName;
        }
        catch (\Exception $ex) { } // Eat the exception will default to the welcome route.

        return \Redirect::route($homeRouteName);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function excel()
    {
        $page_title = "Import Excel";
        $page_description = "Import & Read the Excel file";

        return view('import', compact('page_title', 'page_description'));
    }
    
    private $_error = false;
    
    public function postExcel(Request $request)
    {
        ini_set('memory_limit',-1);
        ini_set('max_execution_time', 300);
        $user_id = Auth::user()->id;
        
        $python_path = storage_path('python');
        
        $destination_path = storage_path('uploads');
        $_arr_excel_ext = array('xls', 'xlsx');
        
        
        if (!$request->file('importExcel')->isValid()) {
            Audit::log(Auth::user()->id, 'ImportExcel', 'No file attached!');
            Flash::error("Sorry, there is no file attached on the form!");
            return redirect()->route('import.excel');
        }
        
        $file = $request->file('importExcel');
        
        if (!in_array($file->getClientOriginalExtension(), $_arr_excel_ext)) {
            Audit::log(Auth::user()->id, 'ImportExcel', 'Failed Read Excel Extension');
        
            Flash::error("Sorry, please use .xls or .xlsx extension!");
            return redirect()->route('import.excel');
        }
        
        $command = "python read_xlsx.py $file $user_id $this->project_id";
        
        exec("cd $python_path && $command 2>&1", $output);
        
        $error_msg = null;
        foreach ($output as $out) {
            $error_msg .= $out . "<br>";
            
        }
        echo $error_msg;
        
        if ($error_msg) {
            //Flash::error($error_msg);
            //return redirect()->route('import.excel');
        }
        
        if (!empty($output)) {
            dd($output);
        }
        
        if(!$error_msg) {
            Audit::log(Auth::user()->id, 'ImportExcel', 'Load Excel Success');
            return redirect()->route('import.excelReviewTemp');
        }
        
        
        
        
        exit;
        
        /*if (!$file->move($destination_path, $file->getClientOriginalName())) {
            Audit::log(Auth::user()->id, 'ImportExcel', 'Load Excel Failed');
            Flash::error("Sorry, error on loading excel!");
            return redirect()->route('import.excel');
        }
        
        Audit::log(Auth::user()->id, 'ImportExcel', 'Load Excel Success');
        */
        
        //$filetmp = storage_path('uploads') . "/" . $file->getClientOriginalName();
        
        
        ### Get column name
        $arrColumnId = null;
        Excel::load($file, function($reader) {

            $results = $reader->first();
            
            foreach($results as $col=>$row)
            {
                if ($col === 0) {
                    break;
                }
                
                $arrColumnName[] = $col;
            }
            
            $this->_arr_column_id = LibColumnName::get_column_id($arrColumnName);
            
        });

        $this->_error = false;
        Excel::filter('chunk')->load($file)->chunk(30, function($reader) use ($request, $arrColumnId)
        {
            $arrColumnId = $this->_arr_column_id;
            //dd($arrColumnId);

            ### Validation
            $results = $reader->all();
            foreach($results as $row)
            {
                if (!isset($row['ActionNo']) || !isset($row['Action No'])) {
                    Flash::error("Sorry, there is no ActionNo column in the file!");
                    return redirect()->route('import.excel');
                }
                
                if (isset($row['Action No'])) {
                    $row['ActionNo'] = $row['Action No'];
                    unset($row['Action No']);
                }
                
                $count = lib_brg_cplx::where(array('ActionNo'=>$row['ActionNo'], 'project_id'=>$this->project_id))->count();
                if ($count > 0) {
                    Flash::error("Sorry, the ActionNo already exists! {$row['ActionNo']}");
                    $this->_error = true;
                    break;
                }
                
                /*
                 $validator = Validator::make(array('ActionNo' => $row['ActionNo']), [
                 'ActionNo' => 'unique:lib_brg_cplx,ActionNo,NULL,id,project_id,' . $this->project_id
                 ]);
                
                 if ($validator->fails()) {
                 return redirect()->route('import.excel')
                 ->withInput()
                 ->withErrors($validator);
                 }
                 */
            }
            
            ### Import Data
            $results = $reader->all();
            if (!$this->_error) {
                foreach($results as $row)
                {
                    $data = new lib_brg_cplx();
                    $data->ActionNo = $row['ActionNo'];
                    $data->project_id = $this->project_id;
                    $data->save();
                    $data_id = $data->id;
                    
                    foreach ($row as $col_name=>$col_value) {
                        try {
                            $col_id = $arrColumnId[$col_name];
                            $arrWhere = array('lib_brg_cplx_id' => $data_id, 'lib_column_name_id' => $col_id);
                            $m = DataColumn::firstOrNew($arrWhere);
                            $m->lib_brg_cplx_id = $data_id;
                            $m->lib_column_name_id = $col_id;
                            $m->data_value = $col_value;
                            $m->save();
                        }
                        catch(Exception $e){
                            //echo $e->getMessage();
                            Flash::error($e->getMessage());
                            return redirect()->route('import.excel');
                        }
                    }
                }
                
                Flash::success("Succes import file.");
            }
            
            
            
        });
        
        return redirect()->route('import.excel');
        
	}
	
	public function getFile()
	{
	    return storage_path('exports') . '/file.csv';
	}
}
