<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;
//use App\Http\Requests;
//use App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use App\Repositories\AuditRepository as Audit;
use App\Repositories\DataRepository;
use GridEncoder;
use Auth;
use Flash;
use View;

class MyController extends Controller
{
    public $project_id;
    
    public function __construct()
    {
        $this->project_id = session('project_id');
        
        //View::share(['project_id' => $this->project_id]);
        //View::share('project_id', $this->project_id);
    }
    
    public function boot() {
        //view()->share('project_id', $this->project_id);
        
    }
}