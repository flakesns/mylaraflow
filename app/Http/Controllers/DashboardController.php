<?php namespace App\Http\Controllers;
use Auth;
use App\Models\Report;

class DashboardController extends MyController
{

    public function index()
    {
        $showThis = false;
        $data['COUNT_UNASSIGN_ACTION'] = 0;
        $data['COUNT_PENDING_RESPONSE'] = 0;
        
        $user_id = Auth::user()->id;
        $user_type = '';
        
        if (Auth::user()->hasRole('coordinator')) {
            $user_id = null;
            $data['COUNT_UNASSIGN_ACTION'] = Report::get_total_unassigned_action($this->project_id);
            $showThis = true;
        }
        
        if (Auth::user()->hasRole('Approver')) {
            $user_type = 'Approver';
        } else {
            $data['COUNT_PENDING_RESPONSE'] = Report::get_total_pending_for_response($this->project_id, $user_id);
            $data['LIST_PENDING_RESPONSE'] = Report::get_latest_pending_response($this->project_id, $user_id);
        }
        
        
        $data['COUNT_CLOSE_CASE'] = Report::get_total_close_case($this->project_id, $user_id);
        $data['COUNT_ON_GOING'] = Report::get_total_on_going($this->project_id, $user_id, $user_type);
        
        $data['LIST_ON_GOING'] = Report::get_latest_on_going($this->project_id, $user_id, $user_type);
        $data['LIST_REJECT'] = Report::get_latest_reject_case($this->project_id, $user_id, $user_type);
        $data['LIST_CLOSE_CASE'] = Report::get_latest_close_case($this->project_id, $user_id, $user_type);
        $data['SHOW_THIS'] = $showThis;
        $data['USER_TYPE'] = $user_type;

        $page_title = "Dashboard";
        $page_description = "Activities summary";

        switch ($user_type) {
            case 'Approver':
                return view('dashboard_approver', compact('page_title', 'page_description'))->with($data);
            break;
            
            default:
                return view('dashboard', compact('page_title', 'page_description'))->with($data);
            break;
        }
        
    }

}
