<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
use Carbon\Carbon;
use App\Models\Data_Flow;
use App\Models\Data_Actionee;

class Data_Response extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'data_response';
    protected $dates = ['deleted_at'];
    protected $fillable = ['lib_brg_cplx_id', 'users_id', 'response', 'revision'];
    
    private $rules = array(
        'response' => 'required',
        'revision' => 'required',
    );

    public $listOfActions = array(
        'Assign Actionee',
        'Response',
        'Submit For Approval',
        '1st Approver',
        '2nd Approver',
        '3rd Approver',
        'Pending for Approval',
        'Pending for 1st Approval',
        'Pending for 2nd Approval',
        'Pending for 3rd Approval',
        'Approve_1',
        'Reject_1',
        'Submit For 2nd Approval',
        'Approve_2',
        'Reject_2',
        'Submit For 3rd Approval',
        'Approve_3',
        'Reject_3',
    );
    
    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);
    
        if ($validator->fails()) {
            $this->error_validator = $validator;
            return false;
        }
    
        return true;
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
    
    public function attachments()
    {
        return $this->hasMany('App\Models\attachments', 'id_lib_brg', 'id');
    }
    
    public function flow()
    {
        return $this->hasMany('App\Models\Data_Flow', 'revision_id', 'id');
    }
    
    public function flowComment1st()
    {
        return $this->hasOne('App\Models\Data_Flow', 'id', 'flow_id_1');
    }
    
    public function flowComment2nd()
    {
        return $this->hasOne('App\Models\Data_Flow', 'id', 'flow_id_2');
    }
    
    public function flowComment3rd()
    {
        return $this->hasOne('App\Models\Data_Flow', 'id', 'flow_id_3');
    }
    
    public function getSubmitForApproval($response_id)
    {
        $arr = array('revision_id'=>$response_id, 'action'=>'Submit For Approval');
        $res = $this->flow()->where($arr)->orderBy('updated_at', 'desc')->first();
        return $res;
    }
    
    public function getLastApprovalStatus($response_id)
    {
        $arr = array('revision_id'=>$response_id, 'action'=>Data_Flow::APPROVAL);
        $res = $this->flow()->where($arr)->orderBy('updated_at', 'desc')->first();
        return $res;
    }
    
    public function is_final_approval($data_id, $response_id)
    {
        $modelActionee = Data_Actionee::where('type', 'like', 'approval_%' )
                        ->where('lib_brg_cplx_id', '=', $data_id)
                        ->orderBy('type', 'desc')->first();
        $last_user_id = $modelActionee->users_id;
        
        $arr = array('revision_id'=>$response_id, 'action'=>Data_Flow::APPROVAL, 'users_id'=>$last_user_id);
        $res = $this->flow()->where($arr)->orderBy('id', 'desc')->first();
        return $res;
    }
    
    public function is_can_approve($data_id, $response_id, $user_id)
    {
        $arr = array('revision_id'=>$response_id, 'action'=>Data_Flow::APPROVAL,
                'remarks'=>Data_Flow::PENDING_FOR_APPROVAL, 'users_id'=>$user_id
                );
        $res = $this->flow()->where($arr)->orderBy('id', 'desc')->first();
        return $res;
    }
}
