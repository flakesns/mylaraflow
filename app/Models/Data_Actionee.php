<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
use Carbon\Carbon;

class Data_Actionee extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'data_actionee';
    protected $dates = ['deleted_at', 'response_due_date'];
    protected $fillable = ['lib_brg_cplx_id', 'users_id', 'type', 'response_due_date', 'remarks'];
    
    private $rules = array(
        'lead_actionee' => 'required',
        'approval_1' => 'required',
        'response_due_date' => 'required',
    );
    
    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);
    
        if ($validator->fails()) {
            $this->error_validator = $validator;
            return false;
        }
    
        return true;
    }
    
    public static function get_data_actionee($data_id)
    {
        $arr['id'] = null;
        $arr['lead_actionee'] = null;
        $response_due_date = null;
        $arr['approval_1'] = null;
        $arr['approval_2'] = null;
        $arr['approval_3'] = null;
        $arr['approval_4'] = null;
        $arr['approval_5'] = null;
        $arr['approval_1_id'] = null;
        $arr['approval_2_id'] = null;
        $arr['approval_3_id'] = null;
        $arr['approval_4_id'] = null;
        $arr['approval_5_id'] = null;
        $arr['userLead'] = null;
        
        $model = Data_Actionee::where('lib_brg_cplx_id', $data_id)->get();
        foreach ($model as $obj) {
            $arr[$obj->type] = $obj->users_id;
            if ($obj->type == 'lead_actionee') {
                $response_due_date = $obj->response_due_date;
            }
        }
        $arr['response_due_date'] = $response_due_date;
        
        return (object)$arr;
    }
    
    public function setResponseDueDateAttribute($value)
    {
        $this->attributes['response_due_date'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    
    public function user_lead_actionee()
    {
        return $this->belongsTo('App\User', 'lead_actionee');
    }
    
    public function user_approval_1()
    {
        return $this->belongsTo('App\User', 'approval_1');
    }
    
    public function user_approval_2()
    {
        return $this->belongsTo('App\User', 'approval_2');
    }
    
    public function user_approval_3()
    {
        return $this->belongsTo('App\User', 'approval_3');
    }

}
