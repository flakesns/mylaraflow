<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
use DB;
use App\User;

class lib_brg_cplx extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'lib_brg_cplx';
    protected $dates = ['deleted_at'];
    protected $fillable = ['ActionNo', 'StudyActionNo', 'Rev', 'Study', 'Phase', 'Node', 'Cause', 'project_id'];
    
    private $rules = array(
        'ActionNo' => 'required|numeric|digits_between:1,5',
    );
    
    public function __construct($user_id = null)
    {
        
    }
    
    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);
        
        if ($validator->fails()) {
            $this->error_validator = $validator;
            return false;
        }
        
        return true;
    }
   /*
    protected static function boot()
    {
        parent::boot();
    
        static::addGlobalScope('myData', function(Builder $builder) {
            $builder->actionee->where('users_id', 17);
        });
    }
    */
    
    public function attachments()
    {
        return $this->hasMany('App\Models\attachments', 'id_lib_brg', 'id');
    }
    
    public function actionee()
    {
        return $this->hasMany('App\Models\Data_Actionee', 'lib_brg_cplx_id', 'id');
    }
    
    public function scopeMyData($query, $type)
    {
        return $query->where('type', $type);
    }
    
    public function project()
    {
        return $this->hasOne('App\Models\Project', 'id', 'project_id');
        //return $this->hasOne('App\Phone', 'foreign_key', 'local_key');
    }
    
    public function data_column()
    {
        return $this->hasMany('App\Models\DataColumn', 'lib_brg_cplx_id', 'id');
    }
    
    /*
     * TODO: make this function available to all case Pending, Approval etc.
     */
    public static function get_model_close_case($project_id, $id = null, $type = null)
    {
        if ($type == null) {
                $query = DB::table('lib_brg_cplx')
                ->select(
                    'lib_brg_cplx.id as lib_brg_cplx_id', 'lib_brg_cplx.ActionNo', 'lib_brg_cplx.group_import_id',
                    'group_import.column_header', 'data_response.*', 'data_response.id as response_id', 'data_response.users_id as response_user_id',
                    'data_flow.updated_at as close_date', 'data_flow.action', 'data_flow.users_id as flow_users_id',
                    'data_flow.revision_id as response_rev_id'
                )
                ->join('data_flow', function ($join) {
                    $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                    ->where('data_flow.action', '=', Data_Flow::CASE_CLOSE)->where('data_flow.remarks', '=', Data_Flow::CASE_CLOSE);
                })
                
                ->join('data_response', 'data_response.id', '=', 'data_flow.revision_id')
                
                ->join('group_import', 'group_import.id', '=', 'lib_brg_cplx.group_import_id');
                
                $query->where('lib_brg_cplx.project_id', $project_id);
                
                if (is_array($id)) {
                    $query->whereIn('lib_brg_cplx.id', $id);
                } else {
                    $query->where('lib_brg_cplx.id', $id);
                }
        
                $query->orderBy('lib_brg_cplx.group_import_id', 'lib_brg_cplx.ActionNo');
                
                $model = $query->get();
        }
        
        if ($type == 'pending_for_approval') {
            
            $query = DB::table('lib_brg_cplx')
                ->select(
                    'lib_brg_cplx.id as lib_brg_cplx_id', 'lib_brg_cplx.ActionNo', 'lib_brg_cplx.group_import_id',
                    'group_import.column_header', 'data_response.*', 'data_response.id as response_id', 'data_response.users_id as response_user_id',
                    'data_flow.updated_at as close_date', 'data_flow.action', 'data_flow.revision_id as response_rev_id'
                )
                
                ->join('group_import', 'group_import.id', '=', 'lib_brg_cplx.group_import_id');
                            
                $query->where('lib_brg_cplx.project_id', $project_id);
                
                $query->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                
                $query->join('users', 'users.id', '=', 'data_actionee.users_id');
                
                $query->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
                $query->whereNotNull('data_response.digital_sign_lead');
                
                #get last action
                $query->join('data_flow', 'data_flow.revision_id', '=', 'data_response.id');
                
                
                $query->whereIn('data_flow.remarks', [Data_Flow::PENDING_FOR_APPROVAL, Data_Flow::APPROVE_STATUS, Data_Flow::REJECT_STATUS] );
                
                
                if ($id) {
                    $query->where('lib_brg_cplx.id', $id);
                }
                
                $query->whereRaw("lib_brg_cplx.id NOT IN (SELECT data_flow.lib_brg_cplx_id FROM data_flow
                                                                            WHERE data_flow.action='Case Close' AND data_flow.lib_brg_cplx_id = lib_brg_cplx.id)");
                
                $query->groupBy('data_response.lib_brg_cplx_id');
                
                $query->orderBy('data_flow.revision_id', 'desc');
                
            
                $model = $query->get();
        }
        
        return $model;
    }
    
    public static function get_visible_column()
    {
        #load visible column
        $modVis = LibColumnName::where('active',1)->get();
        foreach ($modVis as $obj) {
            $arrVisibleColumn[$obj->id] = $obj->column_name;
        }
        
        return $arrVisibleColumn;
    }
    
    public static function get_user_info($id, $field)
    {
        $user = User::find($id);
        return $user->$field;
    }
    
    public static function get_predefine_action_details($arr, $category = 'Print')
    {
        $arr_pre = array();
        
        if (isset($arr['StudyActionNo'])) {
            $key = 'StudyActionNo';
        }
        if (isset($arr['Origin'])) {
            $key = 'Origin';
        }
        
        $query = DB::table('predefine_report_column')->where('key_column', $key)->where('category', $category);
        $model = $query->first();
        
        $columns = $model->columns;
        $arrPre = explode(',', $columns);
        $arrPre = array_map('trim',$arrPre);
        
        foreach ($arrPre as $index => $key) {
            $arr_pre[$key] = $arr[$key];
        }
        
        return $arr_pre;
    }
    
    public static function get_mapping_column($arr, $category = 'Print')
    {
        $modVis = LibColumnName::where('active',1)->get();
        foreach ($modVis as $obj) {
            $current_column = $obj->column_name;
            $mapping_col = $obj->mapping_column;
            if ($mapping_col != '') {
                if(isset($arr[$current_column])) {
                    $arr[$mapping_col] = $arr[$current_column];
                }
            }
        }
        
        return $arr;
    }
    
    public static function get_user_discipline_roles($user_id)
    {
        $query = DB::table('role_user');
        $query->select('role_user.role_id');
        $query->join('roles', 'roles.id', '=', 'role_user.role_id');
        $query->where('role_user.user_id', $user_id);
        $query->where('roles.name', 'like', 'Dis_%');
        $model = $query->get();
        foreach ($model as $obj) {
            $role_disc_id[] = $obj->role_id;
        }
        return $role_disc_id;
    }
     
    public static function list_users_by_roles($roles, $discipline_roles_id=null)
    {
        $lists = array();
               
        $query = DB::table('users');
        $query->select('users.id', 'users.first_name', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS fullname'));
        $query->join('role_user', 'role_user.user_id', '=', 'users.id');
        /*$query->join('roles', function ($join) use ($roles) {
            $join->on('roles.id', '=', 'role_user.role_id')
            ->where('roles.name', '=', $roles);
        });*/
        //$query->join('roles', 'roles.id', '=', 'role_user.role_id');
        //$query->where('roles.name', $roles);
        $query->whereIn('role_user.role_id', $discipline_roles_id);
        $model = $query->get();
        foreach ($model as $obj) {
            //if ($obj->role_name == $roles) {
                $lists[$obj->id] = $obj->fullname;
            //}
        }
        //dd($lists);
        return $lists;
    }
}
