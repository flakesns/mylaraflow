<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
use Carbon\Carbon;

class Data_Flow extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'data_flow';
    protected $dates = ['deleted_at'];
    protected $fillable = ['lib_brg_cplx_id', 'users_id', 'action', 'remarks', 'comments'];
    
    const SUBMIT_FOR_APPROVAL = 'Submit For Approval';
    const PENDING_FOR_APPROVAL = 'Pending For Approval';
    const APPROVAL = 'Approval';
    const APPROVE_STATUS = 'Approve';
    const REJECT_STATUS = 'Reject';
    const CASE_CLOSE = 'Case Close';

    public static function log($user_id, $data_id, $revision_id, $action, $remarks=null)
    {
        /*
        $count = Data_Flow::where( ['lib_brg_cplx_id'=>$data_id, 'revision_id'=>$revision_id, 'action'=>$action] )->count();
        if ($count > 0) {
            return true;
        }
        */
        
        $model = new Data_Flow();
        $model->users_id = $user_id;
        $model->lib_brg_cplx_id = $data_id;
        $model->revision_id = $revision_id;
        $model->action = $action;
        $model->remarks = $remarks;
        $model->save();
        return $model->id;
    }
    
    public static function log_update($user_id, $data_id, $revision_id, $action, $remarks=null, $comments=null)
    {
        $model = Data_Flow::where( ['users_id'=>$user_id, 'lib_brg_cplx_id'=>$data_id, 'revision_id'=>$revision_id, 'action'=>$action] )
                            ->orderBy('updated_at', 'desc')->first();
        $model->remarks = $remarks;
        $model->comments = $comments;
        $model->save();
        return $model->id;
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
    
    public static function get_last_action($data_id, $user_id)
    {
        $model = Data_Flow::where(['lib_brg_cplx_id'=>$data_id, 'users_id'=>$user_id])
                ->orderBy('created_at', 'desc')
                ->limit(1)
                ->first();
        
        return $model;
    }
    
    public static function get_last_approval_status($data_id, $revision_id)
    {
        
    }
}
