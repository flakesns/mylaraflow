<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Data_Flow;

class Report extends Model
{
    public function __construct()
    {
        DB::enableQueryLog();
    }

    public static function get_total_unassigned_action($project_id)
    {
        /*
        $results = DB::select('SELECT COUNT(*) as cnt FROM lib_brg_cplx WHERE project_id = :project_id AND id NOT IN
                                (SELECT lib_brg_cplx_id FROM data_actionee)', ['project_id' => $project_id]);
        
        $res = array_shift($results);
        return $res->cnt;
        */
                
        $query = DB::table('lib_brg_cplx');
        $query->where('lib_brg_cplx.project_id', $project_id);
        
        $query->whereNotIn('id', function ($query) {
                $query->select('lib_brg_cplx_id')->from('data_actionee');
            });

        $res = $query->count();
        return $res;
    }
    
    public static function get_total_pending_for_response($project_id, $user_id = null)
    {
        $query = DB::table('lib_brg_cplx');
        $query->where('lib_brg_cplx.project_id', $project_id);
        
        $query->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
        $query->where('data_actionee.type', 'lead_actionee');
        
        $query->join('users', 'users.id', '=', 'data_actionee.users_id');
        
        $query->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id', 'left outer');
        $query->whereNull('data_response.lib_brg_cplx_id');
        
        if ($user_id) {
            $query->where('data_actionee.users_id', $user_id);
        }

        $res = $query->count();
        return $res;
    }
    /*
    public static function get_total_close_case($project_id, $user_id = null)
    {
        $query = DB::table('lib_brg_cplx');
        $query->where('lib_brg_cplx.project_id', $project_id);
    
        $query->join('data_flow', function ($join) {
                    $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                         ->where('data_flow.action', '=', Data_Flow::CASE_CLOSE)->where('data_flow.remarks', '=', Data_Flow::CASE_CLOSE);
                });
        
        if ($user_id) {
            $query->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
            $query->where('data_response.users_id', $user_id);
        }
    
        $res = $query->count();
        return $res;
    }
    */
    
    public static function get_total_close_case($project_id, $user_id = null)
    {
        DB::enableQueryLog();
        $query = DB::table('lib_brg_cplx');
        $query->select('lib_brg_cplx.id');
        $query->where('lib_brg_cplx.project_id', $project_id);
        $query->where('lib_brg_cplx.case_closed', 1);
    
        $query->join('data_flow', function ($join) use($user_id) {
            $q = $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
            if ($user_id) {
                $q->where('data_flow.users_id', '=', $user_id);
            }
        });
        
        $query->groupBy('lib_brg_cplx.id');

        $res = $query->get();
        
        $res = count($res);
        /*
        $query = DB::getQueryLog();
        $lastQuery = end($query);
        dd($lastQuery);
        */
        return $res;
    }
    
    public static function get_total_on_going($project_id, $user_id = null, $user_type = null)
    {
        DB::enableQueryLog();
        $query = DB::table('lib_brg_cplx');
                
        $query->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
        $query->whereNotNull('data_response.digital_sign_lead');
               
        switch ($user_type) {
            case 'Approver':
                $query->join('data_flow', 'data_flow.revision_id', '=', 'data_response.id');
                $query->where('data_flow.users_id', $user_id);
                $query->whereIn('data_flow.remarks', [Data_Flow::PENDING_FOR_APPROVAL, Data_Flow::APPROVE_STATUS, Data_Flow::REJECT_STATUS] );
            break;
            
            default:
                if ($user_id) {
                    $query->where('data_response.users_id', $user_id);
                }
            break;
        }
        
        $query->where('lib_brg_cplx.project_id', $project_id);
        $query->where('lib_brg_cplx.case_closed', '!=', 1);

        $query->groupBy('lib_brg_cplx.id');
        
        $res = $query->count();
        /*$query = DB::getQueryLog();
        $lastQuery = end($query);
        dd($lastQuery);*/
        return $res;
    }
    
    public static function get_total_pending_for_approval($project_id, $user_id)
    {
        $this->Database->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
        
        $this->Database->join('users', 'users.id', '=', 'data_actionee.users_id');
        
        $this->Database->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
        $this->Database->whereNotNull('data_response.digital_sign_lead');
        
        #get last action
        $this->Database->join('data_flow', 'data_flow.revision_id', '=', 'data_response.id');
        
        
        $this->Database->whereIn('data_flow.remarks', [Data_Flow::PENDING_FOR_APPROVAL, Data_Flow::APPROVE_STATUS, Data_Flow::REJECT_STATUS] );
        
        
        if ($user_id) {
            $this->Database->where('data_actionee.users_id', $user_id);
        }
        
        $this->Database->whereRaw("lib_brg_cplx.id NOT IN (SELECT data_flow.lib_brg_cplx_id FROM data_flow
                                                                            WHERE data_flow.action='Case Close' AND data_flow.lib_brg_cplx_id = lib_brg_cplx.id)");
        
        $this->Database->groupBy('data_response.lib_brg_cplx_id');
        
        $this->orderBy = array(array('data_response.revision', 'desc'));
    }
    
    public static function get_latest_pending_response($project_id, $user_id = null, $limit=5)
    {
        $query = DB::table('lib_brg_cplx');
        $query->select('lib_brg_cplx.id', 'lib_brg_cplx.ActionNo');
        $query->where('lib_brg_cplx.project_id', $project_id);
        
        $query->join('data_actionee', 'data_actionee.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
        $query->where('data_actionee.type', 'lead_actionee');
        
        $query->join('users', 'users.id', '=', 'data_actionee.users_id');
        
        $query->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id', 'left outer');
        $query->whereNull('data_response.lib_brg_cplx_id');
        
        if ($user_id) {
            $query->where('data_actionee.users_id', $user_id);
        }
        
        $res = $query->take($limit)->get();
        return $res;
    }
    
    public static function get_latest_on_going($project_id, $user_id = null, $user_type = null, $limit=5)
    {
        $query = DB::table('lib_brg_cplx');
        $query->select('lib_brg_cplx.id', 'lib_brg_cplx.ActionNo');
        $query->where('lib_brg_cplx.project_id', $project_id);
        $query->where('lib_brg_cplx.case_closed', '!=', 1);
        
        $query->join('data_response', 'data_response.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
        $query->whereNotNull('data_response.digital_sign_lead');
        
        
        switch ($user_type) {
            case 'Approver':
                $query->join('data_flow', 'data_flow.revision_id', '=', 'data_response.id');
                $query->where('data_flow.users_id', $user_id);
                $query->whereIn('data_flow.remarks', [Data_Flow::PENDING_FOR_APPROVAL, Data_Flow::APPROVE_STATUS, Data_Flow::REJECT_STATUS] );
                break;
        
            default:
                if ($user_id) {
                    $query->where('data_response.users_id', $user_id);
                }
                break;
        }
        
        
        
        $query->groupBy('lib_brg_cplx.id');
    
        $res = $query->take($limit)->get();
        return $res;
    }
    
    public static function get_latest_reject_case($project_id, $user_id = null, $user_type = null, $limit=5)
    {
        $query = DB::table('lib_brg_cplx');
        $query->select('lib_brg_cplx.id', 'lib_brg_cplx.ActionNo');
        $query->where('lib_brg_cplx.project_id', $project_id);
        $query->where('lib_brg_cplx.case_closed', '!=', 1);
    
        $query->join('data_flow', function ($join) {
                    $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id')
                         ->where('data_flow.action', '=', Data_Flow::APPROVAL)->where('data_flow.remarks', '=', Data_Flow::REJECT_STATUS);
                });

        switch ($user_type) {
            case 'Approver':
                $query->where('data_flow.users_id', $user_id);
                break;
        
            default:
                if ($user_id) {
                    $query->join('data_response', 'data_response.id', '=', 'data_flow.revision_id');
                    $query->where('data_response.users_id', $user_id);
                }
            break;
        }
        
        $query->groupBy('lib_brg_cplx.id');
            
        $res = $query->take($limit)->get();
        return $res;
    }
    
    public static function get_latest_close_case($project_id, $user_id = null, $user_type = null, $limit=5)
    {
        $query = DB::table('lib_brg_cplx');
        $query->select('lib_brg_cplx.id', 'lib_brg_cplx.ActionNo');
        $query->where('lib_brg_cplx.project_id', $project_id);
        $query->where('lib_brg_cplx.case_closed', 1);
        
        $query->join('data_flow', function ($join) use($user_id) {
            $q = $join->on('data_flow.lib_brg_cplx_id', '=', 'lib_brg_cplx.id');
            if ($user_id) {
                $q->where('data_flow.users_id', '=', $user_id);
            }
        });

        $query->groupBy('lib_brg_cplx.id');
        
        $res = $query->take($limit)->get();
        return $res;
    }
}
