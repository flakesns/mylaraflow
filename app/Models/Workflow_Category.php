<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Workflow_Category extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'workflow_category';
    protected $dates = ['deleted_at'];
    protected $fillable = ['category_name', 'category_desc', 'lead_actionee', 'approval_1', 'approval_2', 'approval_3'];
    
    protected $casts = [
        //'lead_actionee' => 'array',
        //'approval' => 'array',
    ];
    
    public static function list_approval_column()
    {
        return array('lead_actionee', 'approval_1', 'approval_2', 'approval_3', 'approval_4', 'approval_5');
    }
    
    private $rules = array(
        'category_name' => 'required',
        'lead_actionee' => 'required',
        'approval_1' => 'required',
    );
    
    public function validate($data)
    {
        $validator = Validator::make($data, $this->rules);
    
        if ($validator->fails()) {
            $this->error_validator = $validator;
            return false;
        }
    
        return true;
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'lead_actionee');
    }
    
    public function user1()
    {
        return $this->belongsTo('App\User', 'approval_1');
    }
    
    public function user2()
    {
        return $this->belongsTo('App\User', 'approval_2');
    }
    
    public function user3()
    {
        return $this->belongsTo('App\User', 'approval_3');
    }
    
    public function getApprovalNameAttribute()
    {
        
    }
}
