<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\lib_brg_cplx;
use App\Models\DataColumn;
use App\Models\TempData;
use App\Models\Data_Actionee;
use App\User;
use App\Jobs\SendEmailRequestResponse;
use App\Http\Controllers\Controller;
use Mail;
use Auth;

class TempAction extends Model
{
    protected $table = 't_action_no';
    
    public function temp_data()
    {
        return $this->hasMany('App\Models\TempData', 't_action_no_id', 'id');
    }
    
    public function save_only($project_id, $user_id, $assign = null)
    {
        $model = TempAction::where(['user_id'=>$user_id, 'project_id'=>$project_id])->get();
        
        foreach ($model as $obj) {
            $temp_id = $obj->id;
            $m = new lib_brg_cplx();
            $m->ActionNo = $obj->ActionNo;
            $m->project_id = $project_id;
            $m->group_import_id = $obj->group_import_id;
            if ($m->save()) {
                $lib_brg_cplx_id = $m->id;
                $tempData = TempData::where(['t_action_no_id'=>$temp_id])->get();
                foreach ($tempData as $obj2) {
                    
                    $response_due_date = '';
                    
                    $n = new DataColumn();
                    $n->lib_brg_cplx_id = $lib_brg_cplx_id;
                    $n->lib_column_name_id = $obj2->lib_column_name_id;
                    $n->data_value = $obj2->data_value;
                    if ($n->save()) {

                        if (strtolower($obj2->column_name->column_name) == 'actionee') {
                            #validate workflow is exists
                            $modelLib = lib_brg_cplx::findOrFail($lib_brg_cplx_id);
                            $wf_category = $obj2->data_value;
                            $modelWf = $this->_get_actionee_id($wf_category);
                            if (!$modelWf) {
                                $modelLib->workflow_not_found = 1;
                                $modelLib->save();
                                continue;
                            }
                            
                            $user_lead_id = $modelWf->lead_actionee;
                            $wf_cat_id = $modelWf->id;
                            
                            #update lib if workflow category found
                            $modelLib->workflow_category_id = $wf_cat_id;
                            $modelLib->save();
                        }
                        
                        if (strtolower($obj2->column_name->column_name) == 'discipline') {
                            $modelLib = lib_brg_cplx::findOrFail($lib_brg_cplx_id);
                            $modelLib->discipline = $obj2->data_value;
                            $modelLib->save();
                        }
                        
                        /*
                        #if assign, send mail to predefine workflow actionee from excel file
                        if ($assign && strtolower($obj2->column_name->column_name) == 'actionee') {
                            
                            #validate workflow is exists
                            $modelLib = lib_brg_cplx::findOrFail($lib_brg_cplx_id);
                            $wf_category = $obj2->data_value;
                            $modelWf = $this->_get_actionee_id($wf_category);
                            $user_lead_id = $modelWf->lead_actionee;
                            if (!$user_lead_id) {
                                $modelLib->workflow_not_found = 1;
                                $modelLib->save();
                                continue;
                            }
                            
                            $wf_cat_id = $modelWf->id;
                            
                            #update lib if workflow category found
                            $modelLib->workflow_category_id = $wf_cat_id;
                            $modelLib->save();
                            
                            #Update actionee table
                            $u = $this->_update_list_actionee($lib_brg_cplx_id, $wf_cat_id);
              
                            #send mail
                            $user = User::findOrFail($user_lead_id);
                            $model = lib_brg_cplx::findOrFail($lib_brg_cplx_id);
                            $r['project_name'] = $model->project->project_name;
                            $r['ActionNo'] = $model->ActionNo;
                            $r['response_due_date'] = $response_due_date;
                            $r['url'] = url();
                            $subject = "Requesting response for ActionNo: " . $model->ActionNo;
                            
                            Mail::queue('emails.request_response', ['user'=>$user, 'model'=>$model, 'request'=>$r], function ($message) use ($user, $subject) {
                                $message->from('ams@aramis-gr.com', 'LaraFlow');
                                $message->to($user->email, $user->name);
                                $message->subject($subject);
                            });
                           
                        }
                        */
                        
                    }
                }
                
                #if assign, send mail to predefine workflow actionee from excel file
                if ($assign && $modelWf) {

                    #Update actionee table
                    $u = $this->_update_list_actionee($lib_brg_cplx_id, $wf_cat_id);
                    
                    #send mail
                    $user = User::findOrFail($user_lead_id);
                    $model = lib_brg_cplx::findOrFail($lib_brg_cplx_id);
                    $r['project_name'] = $model->project->project_name;
                    $r['ActionNo'] = $model->ActionNo;
                    $r['response_due_date'] = $response_due_date;
                    $r['url'] = url();
                    $subject = "Requesting response for ActionNo: " . $model->ActionNo;
                    
                    Mail::queue('emails.request_response', ['user'=>$user, 'model'=>$model, 'request'=>$r], function ($message) use ($user, $subject) {
                        $message->from('ams@aramis-gr.com', 'LaraFlow');
                        $message->to($user->email, $user->name);
                        $message->subject($subject);
                    });
                }
            }
        }
        
        $model = TempAction::where(['user_id'=>$user_id, 'project_id'=>$project_id])->delete();
        return true;
    }
    /*
    protected function sendRequestResponse($user_id, $data)
    {
        $user = User::findOrFail($user_id);
        $data['user'] = $user;

        $this->dispatch(new SendEmailRequestResponse($user, $data));
    }
    */
    
    private function _get_actionee_id($cat_name)
    {
        $m = Workflow_Category::where(['category_name'=>$cat_name])->first();
        if (!$m) {
            return false;
        }
        return $m;
    }
    
    private function _update_list_actionee($data_id, $workflow_id)
    {
        $deletedRows = Data_Actionee::where('lib_brg_cplx_id', $data_id)->forceDelete();
        
        $arrValidKey = Workflow_Category::list_approval_column();
        
        $modelWorkflow = Workflow_Category::find($workflow_id);
        
        foreach ($arrValidKey as $key) {
            if ($modelWorkflow->$key == '' || $modelWorkflow->$key == 0) {
                continue;
            }
            $newModel = new Data_Actionee();
            $newModel->lib_brg_cplx_id = $data_id;
            $newModel->type = $key;
            $newModel->users_id = $modelWorkflow->$key;
            $newModel->save();
            
            if ($key == 'lead_actionee') {
                $user = User::find($modelWorkflow->lead_actionee);
                $lead_name = $user->fullname;
                Data_Flow::log(Auth::user()->id, $data_id, null, "Assign Actionee", "Assign to $lead_name");
                Data_Flow::log($modelWorkflow->lead_actionee, $data_id, null, "Response", "Pending");
            }
        }
        
    }
    
}
