<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataColumn extends Model
{
    protected $table = 'data_column';
    protected $fillable = array('lib_brg_cplx_id', 'lib_column_name_id', 'data_value');
    
    public function column_name()
    {
        return $this->belongsTo('App\Models\LibColumnName', 'lib_column_name_id', 'id');
    }
}
