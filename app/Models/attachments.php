<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class attachments extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'attachments';
    protected $dates = ['deleted_at'];
    protected $fillable = ['id_lib_brg', 'file_name'];
}
