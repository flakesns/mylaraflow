<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Project extends Model
{
    use SoftDeletes;
    public $error_validator;
    protected $table = 'project';
    protected $dates = ['deleted_at'];
    protected $fillable = ['project_name', 'project_desc'];
}
