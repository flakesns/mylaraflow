<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempData extends Model
{
    protected $table = 't_data_column';
    
    public function column_name()
    {
        return $this->belongsTo('App\Models\LibColumnName', 'lib_column_name_id', 'id');
    }
}
