<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LibColumnName extends Model
{
    protected $table = 'lib_column_name';
    
    public static function save_column($col_name)
    {
        $m = new LibColumnName();
        $m->column_name = $col_name;
        $m->save();
        return $m->id;
    }
    
    public static function get_column_id(array $arrColumnName)
    {
        $arr = array();
        //DB::connection()->enableQueryLog();
        $m = LibColumnName::whereIn('column_name', $arrColumnName)->get();
        //$query = DB::getQueryLog();
        //$lastQuery = end($query);
        //print_r($lastQuery);
        
        foreach ($m as $obj) {
            $arr[$obj->column_name] = $obj->id;
        }

        foreach ($arrColumnName as $col_name) {
            if (!isset($arr[$col_name])) {
                $arr[$col_name] = self::save_column($col_name);
            }
        }
        
        return $arr;
    }
}
