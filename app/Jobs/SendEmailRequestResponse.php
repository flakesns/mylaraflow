<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRequestResponse extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $data;
    protected $user;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $model = $this->data['model'];
        $mailer->send('emails.request_response', [ 'model'=>$model, 'user'=>$this->user], function ($message) use ($user, $model) {
            $message->from('ams@aramis-gr.com', 'LaraFlow');
            $message->to($user->email, $user->name);
            $message->subject("Requesting response for ActionNo: " . $model->ActionNo);
        });
        //echo "pppp";
    }
}
