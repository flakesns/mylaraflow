
<p>
Hi <?php echo $user->username ?>,
</p>

<p>
A new Action have been submitted for your approval.
</p>

<table>
    <thead>

    </thead>

    <tbody>
        <tr><td>ActionNo</td><td>{{ $model->ActionNo }}</td></tr>
        <tr><td>Revision</td><td>{{ $modelResponse->revision }}</td></tr>
        <tr><td>Response</td><td>{{ $modelResponse->response }}</td></tr>
        <tr><td>Approval</td><td>{{ $request->approval_status }}</td></tr>
        <tr><td>Comments</td><td>{{ $modelFlow->comments }}</td></tr>
    </tbody>
</table>

<p>
Kindly please visit this url <a href='http://lara5.com'>http://lara5.com</a> for your action.
</p>
