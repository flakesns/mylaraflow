
<style>
/* cellpadding */
th, td { padding: 10px; }

/* cellspacing */
table { border-collapse: separate; border-spacing: 5px; } /* cellspacing="5" */
table { border-collapse: collapse; border-spacing: 0; }   /* cellspacing="0" */

/* valign */
th, td { vertical-align: middle; }

/* align (center) */
/*table { margin: 0 auto; }*/
</style>

<p>
Hi <?php echo $user->username ?>,
</p>

<p>
Congratulation, your ActionNo have been <strong>APPROVED</strong>!
</p>

<table border="1">
    <tbody>
        <tr><td>ActionNo</td><td>{{ $model->ActionNo }}</td></tr>
        <tr><td>Revision</td><td>{{ $modelResponse->revision }}</td></tr>
        <tr><td>Response</td><td>{{ $modelResponse->response }}</td></tr>
        <tr><td>Approval</td><td>{{ $request->approval_status }}</td></tr>
        <tr><td>Comments</td><td>{{ $modelFlow->comments }}</td></tr>
    </tbody>
</table>

<p>
Kindly please login to this site <a href='{{ $request->url }}'>{{ $request->url }}</a> for your action.
</p>
