@extends('layouts.email')

@section('content')

<style>
/* cellpadding */
th, td { padding: 10px; }

/* cellspacing */
table { border-collapse: separate; border-spacing: 5px; } /* cellspacing="5" */
table { border-collapse: collapse; border-spacing: 0; }   /* cellspacing="0" */

/* valign */
th, td { vertical-align: middle; }

/* align (center) */
/*table { margin: 0 auto; }*/
</style>

<p>
Hi <?php echo $user->username ?>,
</p>

<p>
A new Action have been assign to you by the Coordinator. We are waiting your response for this action.
</p>

<?php
$url = (isset($request['url'])) ? $request['url'] : $request->url;
?>
<table border="1">

        <tr><th>Project</th><td><?php echo (isset($request['project_name'])) ? $request['project_name'] : $model->project->project_name ?></td></tr>
        <tr><th>ActionNo</th><td><?php echo (isset($request['ActionNo'])) ? $request['ActionNo'] : $model->ActionNo ?></td></tr>
        <tr><th>Response Due Date</th><td><?php echo (isset($request['response_due_date'])) ? $request['response_due_date'] : $request->response_due_date ?></td></tr>

</table>

<p>
Kindly please login to this site <a href="<?php echo $url ?>"><?php echo $url ?></a> for your action.
</p>

@endsection
