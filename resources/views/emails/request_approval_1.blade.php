
<p>
Hi <?php echo $user->username ?>,
</p>

<p>
A new Action have been submitted for your approval.
</p>

<table>
    <thead>

    </thead>

    <tbody>
        <tr><th>Project</th><td>{{ $model->project->project_name }}</td></tr>
        <tr><th>ActionNo</th><td>{{ $model->ActionNo }}</td></tr>
        <tr><th>Revision</th><td>{{ $modelResponse->revision }}</td></tr>
        <tr><th>Response</th><td>{{ $modelResponse->response }}</td></tr>
    </tbody>
</table>

<p>
Kindly please visit this url <a href='http://lara5.com'>http://lara5.com</a> for more details.
</p>
