<?php
$approver_number = 1;
?>
<div class="row">
  <div class="col-md-12">

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>

                    </thead>

                    <tbody>

                      <tr>
                          <th style='width:200px'>Revision</th>
                          <td>{{ $modelResponse->revision }}</td>
                      </tr>
                      <tr>
                          <th>Response</th>
                          <td>{{ $modelResponse->response }}</td>
                      </tr>
                      <tr>
                          <th>Attachment</th>
                          <td>
                            @foreach($modelResponse->attachments as $attach)
                                {!! link_to( route('lib-data.getFile', $attach->filename) . '/' . $attach->file_name, $attach->file_name) !!}
                                <br>
                            @endforeach
                          </td>
                      </tr>
                      <tr>
                          <th>Actionee</th>
                          <td>{{ $modelResponse->user->fullname }}</td>
                      </tr>
                      <tr>
                          <th>Submission</th>
                          <td>{{ $modelResponse->created_at->format('d/m/Y H:i:s') }}</td>
                      </tr>
                      <tr>
                            <th>Digital Sign Actionee</th>
                            <td>
                              {{-- $modelResponse->digital_sign_lead --}}

                              @if ($compare_sign_lead)
                                  <img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid Sign</i>
                              @else
                                  <img src="{{url('/images/false.png')}}" alt="Image"/> <i>Invalid Sign</i>
                              @endif

                            </td>
                      </tr>

                      @if ($modelResponse->flow_id_1 != '')
                          <?php $approver_number = 2; ?>
                        <tr>
                            <th>1st Approver Status</th>
                            <td>
                                <?php $label = 'success'; ?>
                                  @if ($modelResponse->flowComment1st->remarks == 'Reject')
                                      <?php $label = 'danger'; ?>
                                  @endif
                                  <span class="label label-{{ $label }}">{{ $modelResponse->flowComment1st->remarks }}</span>
                            </td>
                        </tr>

                        <tr>
                            <th>1st Approver Digital Sign</th>
                            <td>
                                {{-- $modelResponse->digital_sign_approval_1 --}}
                                @if (!$compare_sign_1st)
                                    <img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid Sign</i>
                                @else
                                    <img src="{{url('/images/false.png')}}" alt="Image"/> <i>Invalid Sign</i>
                                @endif
                            </td>
                        </tr>

                        <tr>
                              <th>1st Approver Comments</th>
                              <td>
                                     {{ $modelResponse->flowComment1st->comments }}
                              </td>
                        </tr>
                      @endif

                      @if ($modelResponse->flow_id_2 != '')
                          <?php $approver_number = 3; ?>
                        <tr>
                            <th>2nd Approver Status</th>
                            <td>
                                <?php $label = 'success'; ?>
                                  @if ($modelResponse->flowComment2nd->remarks == 'Reject')
                                      <?php $label = 'danger'; ?>
                                  @endif
                                  <span class="label label-{{ $label }}">{{ $modelResponse->flowComment2nd->remarks }}</span>
                            </td>
                        </tr>

                        <tr>
                            <th>2nd Approver Digital Sign</th>
                            <td>
                                {{-- $modelResponse->digital_sign_approval_2 --}}
                                @if (!$compare_sign_2nd)
                                    <img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid Sign</i>
                                @else
                                    <img src="{{url('/images/false.png')}}" alt="Image"/> <i>Invalid Sign</i>
                                @endif
                            </td>
                        </tr>

                        <tr>
                              <th>2nd Approver Comments</th>
                              <td>
                                     {{ $modelResponse->flowComment2nd->comments }}
                              </td>
                        </tr>
                      @endif

                      @if ($modelResponse->flow_id_3 != '')
                        <tr>
                            <th>3rd Approver Status</th>
                            <td>
                                <?php $label = 'success'; ?>
                                  @if ($modelResponse->flowComment3rd->remarks == 'Reject')
                                      <?php $label = 'danger'; ?>
                                  @endif
                                  <span class="label label-{{ $label }}">{{ $modelResponse->flowComment3rd->remarks }}</span>
                            </td>
                        </tr>

                        <tr>
                            <th>3rd Approver Digital Sign</th>
                            <td>
                                {{-- $modelResponse->digital_sign_approval_3 --}}
                                @if (!$compare_sign_3rd)
                                    <img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid Sign</i>
                                @else
                                    <img src="{{url('/images/false.png')}}" alt="Image"/> <i>Invalid Sign</i>
                                @endif
                            </td>
                        </tr>

                        <tr>
                              <th>3rd Approver Comments</th>
                              <td>
                                     {{ $modelResponse->flowComment3rd->comments }}
                              </td>
                        </tr>
                      @endif

                      {{--@if ($final_approval->remarks == 'Pending For Approval')--}}
                      @if ($is_can_approve)
                          <tr><td colspan="2"><br><div class="box box-primary"></div></td></tr>
                          <tr>
                                <th>Your Comments</th>
                                <td>
                                       {!! Form::textarea('comments', null, ['class' => 'form-control']) !!}
                                </td>
                          </tr>

                          <tr>
                              <td></td>
                              <td>
                                {!! Form::hidden('approver_number', $approver_number) !!}
                                <br>
                                <div class="form-group">
                                    <button type="submit" name="approval_status" value="Approve" class="btn btn-primary">
                                        <i class="glyphicon glyphicon-ok"></i> Approve
                                    </button>
                                    <button type="submit" name="approval_status" value="Reject" class="btn btn-danger">
                                        <i class="glyphicon glyphicon-minus"></i> Reject
                                    </button>
                                    <a href="{!! route('lib-data.pendingForApproval') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                                </div>
                              </td>
                          </tr>
                      @endif

                    </tbody>
                </table>
              </div> <!-- table-responsive -->

    </div>
</div><!-- /.row -->
