<?php
use App\Models\Data_Flow;

$current_login_user = Auth::user()->id;

$locked = false;
$readonly = '';
if (Auth::user()->hasRole('coordinator')) {
  $readonly = 'readonly';
}
$last_revision = 1;
$last_response = '';

if (isset($modelResponseHistory)) {
  $modelResponse = $modelResponseHistory;
}
?>

<div class="row">
  <div class="col-md-12">
        <div class="box box-primary">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Revision</th>
                            <th>Response</th>
                            <th>Attachment</th>
                            <th>Actionee</th>
                            <th>Submission</th>
                            <!--th>Digital Sign</th-->
                            <th>Approval Status</th>
                            <th>Approval Comments</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($modelResponse as $cat)
                            <?php
                              $locked = false;
                              $data_id = $cat->lib_brg_cplx_id;
                              $response_id = $cat->id;
                              $last_revision = $cat->revision;
                              $last_response = $cat->response;

                              $submit = $cat->getSubmitForApproval($response_id); //$cat->flow()->where(['revision_id'=>$response_id, 'action'=>'Submit For Approval'])->orderBy('updated_at', 'desc')->first();
                              if ($submit) {
                                  $locked = true;
                              }

                              $lastStatus = $cat->getLastApprovalStatus($response_id);
                              $approval_status = '';
                              $approval_comments = '';
                              if ($lastStatus) {
                                  $approval_status = $lastStatus->remarks;
                                  $approval_comments = $lastStatus->comments;
                                  if ($approval_status == Data_Flow::REJECT_STATUS) {
                                      $locked = false;
                                      $last_revision = 2;
                                      $last_response = null;
                                  }
                              }

                              if ($cat->users_id != $current_login_user) {
                                  $locked = true;
                              }


                              $final_approval = $cat->is_final_approval($data_id, $response_id);
                            ?>
                          <tr>
                              <td>{{ $cat->revision }}</td>
                              <td>{{ $cat->response }}</td>
                              <td>
                                  @foreach($cat->attachments as $attach)
                                      @if (!$locked && !$submit)
                                          <i class="fa fa-trash-o text-muted"></i>
                                      @endif
                                      {!! link_to( route('lib-data.getFile', $attach->filename) . '/' . $attach->file_name, $attach->file_name) !!}
                                      <br>
                                  @endforeach
                              </td>
                              <td>{{ $cat->user->fullname }}</td>
                              <td>
                                  @if ($submit)
                                    {{ $submit->updated_at->format('d/m/Y H:i:s') }}
                                  @else
                                    {!! Form::button('Submit For Approval', ['class' => 'btn btn-warning', 'onclick' => "confirmSubmit($response_id)"]) !!}
                                  @endif
                              </td>
                              <!--td>
                                {{ $cat->digital_sign_lead }}
                              </td-->
                              <td>
                                {{--@if ($final_approval) --}}

                                  <?php $label = 'success'; ?>
                                    @if ($approval_status == 'Reject')
                                        <?php $label = 'danger'; ?>
                                    @endif

                                    <span class="label label-{{ $label }}">{{ $approval_status }}{{-- $final_approval->remarks --}}</span>
                                {{--@endif--}}

                              </td>
                              <td>
                                  {{ $approval_comments }}
                              </td>
                              <td style="text-align: right">
                                @if ($final_approval && $final_approval->remarks == 'Approve')
                                    {!! Form::button('Print', ['class' => 'btn btn-primary', 'onclick' => "printData($data_id, $response_id)"]) !!}
                                @endif

                                @if (!$locked && !$submit)
                                    <i class="fa fa-trash-o text-muted"></i>
                                @endif
                              </td>
                          </tr>
                        @endforeach

                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
    </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-12">
      <br><br>
    </div>
</div>

@if (!$locked)
  <div class="row">
        <div class="col-md-6">
              <div class="form-group">
                  {!! Form::label('Revision') !!}
                  {!! Form::text('revision', $last_revision, ['class' => 'form-control' , 'readonly']) !!}
              </div>
        </div>
  </div>

  <div class="row">
        <div class="col-md-6">
              <div class="form-group">
                  {!! Form::label('Response') !!}
                  {!! Form::textarea('response', $last_response, ['class' => 'form-control']) !!}
              </div>
        </div>
  </div>

  <div class="row">
        <div class="col-md-6">
              <div class="form-group">
                  {!! Form::label('Attachment') !!}
                  {!! Form::file('images[]', array('multiple'=>true)) !!} <br>
                  <p class='small'><i>Use shift key to select multiple files.</i></p><br>
              </div>
        </div>
  </div>

  <div class="form-group">
      <button type="submit" class="btn btn-primary">
          <i class="fa fa-plus"></i> Save
      </button>
      <a href="{!! route('lib-data.listData') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
  </div>
@endif


@section('body_bottom')
<script type="text/javascript">
function confirmSubmit(response_id)
{
  location.href = "<?php echo route('lib-data.confirmSubmit', ['id'=>$model->id]) ?>/"+response_id;
}

function printData(data_id, response_id)
{
  location.href = "<?php echo route('lib-data.printData', ['id'=>$model->id]) ?>/"+response_id;
}
</script>
@endsection
