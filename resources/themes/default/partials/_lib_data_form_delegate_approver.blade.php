
<div class="row">
  <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('Approver 1') !!}
          {!! Form::select('approval_1_display', $users, $modelWorkflow->approval_1_id, ['class' => 'form-control']) !!}
          {!! Form::hidden('approval_1', $modelWorkflow->approval_1_id) !!}
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('Approver 2') !!}
          {!! Form::select('approval_2_display', $users, $modelWorkflow->approval_2_id, ['class' => 'form-control']) !!}
          {!! Form::hidden('approval_2', $modelWorkflow->approval_2_id) !!}
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('Approver 3') !!}
          {!! Form::select('approval_3_display', $users, $modelWorkflow->approval_3_id, ['class' => 'form-control']) !!}
          {!! Form::hidden('approval_3', $modelWorkflow->approval_3_id) !!}
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('Approver 4') !!}
          {!! Form::select('approval_4_display', $users, $modelWorkflow->approval_4_id, ['class' => 'form-control']) !!}
          {!! Form::hidden('approval_4', $modelWorkflow->approval_4_id) !!}
        </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('Approver 5') !!}
          {!! Form::select('approval_5_display', $users, $modelWorkflow->approval_5_id, ['class' => 'form-control']) !!}
          {!! Form::hidden('approval_5', $modelWorkflow->approval_5_id) !!}
        </div>
  </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-plus"></i> Save & Send Notification
    </button>
    <a href="{{ URL::previous() }}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
</div>

<script type="text/javascript">
function load_wf_delegate(obj)
{
  var value = obj.value;
  var url = "<?php echo route('lib-data.edit', ['id'=>$model->id]) ?>/delegate/" + value;
  location.href = url;
}
</script>

