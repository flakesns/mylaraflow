<?php
$readonly = '';
if (!Auth::user()->hasRole('coordinator')) {
  $readonly = 'readonly';
}
?>
<div class="row">
      <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('ActionNo') !!}
                {!! Form::text('ActionNo', null, ['class' => 'form-control', $readonly]) !!}
            </div>
      </div>
      <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('StudyActionNo', 'StudyActionNo') !!}
                {!! Form::text('StudyActionNo', null, ['class' => 'form-control', $readonly]) !!}
            </div>
      </div>
</div>


<div class="row">
  <div class="col-md-6">
      <div class="form-group">
          {!! Form::label('Study') !!}
          {!! Form::text('Study', null, ['class' => 'form-control', $readonly]) !!}
      </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
        {!! Form::label('Phase') !!}
        {!! Form::text('Phase', null, ['class' => 'form-control', $readonly]) !!}
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-6">
    <div class="form-group">
        {!! Form::label('Node') !!}
        {!! Form::text('Node', null, ['class' => 'form-control', $readonly]) !!}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
        {!! Form::label('SafeGuard') !!}
        {!! Form::text('SafeGuard', null, ['class' => 'form-control', $readonly]) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('Cause') !!}
            {!! Form::text('Consequence', null, ['class' => 'form-control', $readonly]) !!}
        </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
        {!! Form::label('Location', 'Location') !!}
        {!! Form::text('Location', null, ['class' => 'form-control', $readonly]) !!}
    </div>
  </div>
</div>

<div class="row">
      <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('Recommendation') !!}
                {!! Form::text('Recommendation', null, ['class' => 'form-control', $readonly]) !!}
            </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('HazardNo') !!}
            {!! Form::text('HazardNo', null, ['class' => 'form-control', $readonly]) !!}
        </div>
      </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
        {!! Form::label('DocumentNo') !!}
        {!! Form::text('DocumentNo', null, ['class' => 'form-control', $readonly]) !!}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
        {!! Form::label('DocumentTitle') !!}
        {!! Form::text('DocumentTitle', null, ['class' => 'form-control', $readonly]) !!}
    </div>
  </div>
</div>

@if ($readonly == '')
<div class="form-group">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-plus"></i> Save
    </button>
    <a href="{!! route('lib-data.listData') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
</div>
@endif
