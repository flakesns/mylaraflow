
<div class="row">
  <div class="col-md-12">
      <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Latest Action</h3>
        </div>
        <div class="form-group">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Lead Actionee<br>
                              <span class="label label-warning">{{ $modelWorkflow->userLead }}</span>
                            </th>
                            <th>1st Approver<br>
                              <span class="label label-warning">{{ $modelWorkflow->approval_1 }}</span>
                            </th>
                            <th>2nd Approver<br>
                                <span class="label label-warning">{{ isset($modelWorkflow->approval_2) ? $modelWorkflow->approval_2 : '' }}</span>
                            </th>
                            <th>3rd Approver<br>
                                <span class="label label-warning">{{ isset($modelWorkflow->approval_3) ? $modelWorkflow->approval_3 : '' }}</span>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>{{ isset($modelWorkflow->lead_latest_action) ?  $modelWorkflow->lead_latest_action->updated_at->format('d/m/Y') .' '. $modelWorkflow->lead_latest_action->remarks : '' }}</td>
                            <td>{{ isset($modelWorkflow->approval_1_latest_action) ?  $modelWorkflow->approval_1_latest_action->updated_at->format('d/m/Y') .' '. $modelWorkflow->approval_1_latest_action->remarks : '-' }}</td>
                            <td>{{ isset($modelWorkflow->approval_2_latest_action) ? $modelWorkflow->approval_2_latest_action->updated_at->format('d/m/Y') .' '. $modelWorkflow->approval_2_latest_action->remarks : '-' }}</td>
                            <td>{{ isset($modelWorkflow->approval_3_latest_action) ? $modelWorkflow->approval_3_latest_action->updated_at->format('d/m/Y') .' '. $modelWorkflow->approval_3_latest_action->remarks : '-' }}</td>
                        </tr>
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
      </div>
    </div>
</div><!-- /.row -->

<div class="row"><br></div>

<div class="row">
  <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Activities</h3>
        </div>
        <div class="form-group">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Actionee</th>
                            <th>Action</th>
                            <th>Remarks</th>
                            <th>Date</th>
                        </tr>
                    </thead>

                    <tbody>
                      @foreach($modelStatus as $cat)
                        <tr>
                            <td>{{ $cat->user->fullname }}</td>
                            <td>{{ $cat->action }}</td>
                            <td>{{ $cat->remarks }}</td>
                            <td>{{ $cat->created_at->format('d/m/Y H:i:s') }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
      </div>
    </div>
</div><!-- /.row -->
