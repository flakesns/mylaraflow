<div class="text-content">
      <div class="span7 offset1">
          @if(Session::has('success'))
            <div class="alert-box success">
              <h2>{!! Session::get('success') !!}</h2>
            </div>
          @endif

          <h3>Attached Files</h3>

          <br>
          <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>File</th>
                        <th style="text-align: right">{!! trans('admin/roles/general.columns.actions')  !!}</th>
                    </tr>
                    @foreach($attachments as $attach)
                        <tr>
                            <td>{!! link_to( route('lib-data.getFile', $attach->filename) . '/' . $attach->file_name, $attach->file_name) !!} </td>
                            <td style="text-align: right">
                                <i class="fa fa-trash-o text-muted"></i>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div><!-- /.table-responsive -->

          <br>
          <h4>Add File</h4>
          {{-- {!! Form::open(array('url'=>'apply/multiple_upload','method'=>'POST', 'files'=>true)) !!} --}}
            <div class="control-group">
              <div class="controls">
                {!! Form::file('images[]', array('multiple'=>true)) !!} <br>
              <p class='small'><i>Use shift key to select multiple files.</i></p><br>
              </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-plus"></i> Save
                </button>
                <a href="{!! route('lib-data.listData') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
            </div>
    </div>
</div>
