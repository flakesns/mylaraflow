<?php
$readonly = '';
if (!Auth::user()->hasRole('coordinator')) {
  $readonly = 'readonly';
}
?>


<div class="row">
        @foreach ($data_cols as $temp)
            <div class="col-md-4">
                <dl>
                      <dt>{{ $arrColumnHeader[$temp->lib_column_name_id] }}</dt>
                      <dd>{{ $temp->data_value }}</dd>
                </dl>
                
                
            </div>
        @endforeach
</div>

<div class="form-group">
    <a href="{{ URL::previous() }}" title="Close" class='btn btn-default'>Close</a>
</div>