@extends('layouts.master')

@section('head_extra')

@endsection


@section('content')
<div class='row'>
    <div class='col-md-12'>
        <!-- Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $page_title }}</h3>

                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <!-- div id="btn-toolbar" class="section-header btn-toolbar" role="toolbar">

              <div id="btn-group-1" class="btn-group">
                {!! Form::button('<i class="fa fa-plus"></i> New', array('id' => 'btn-new', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => 'Add a new book')) !!}
                {!! Form::button('<i class="fa fa-refresh"></i> Refresh', array('id' => 'btn-refresh', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => 'Refresh grid data')) !!}
              </div>
              <div id="btn-group-2" class="btn-group">
                {!! Form::button('<i class="fa fa-edit"></i> Edit', array('id' => 'btn-edit', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => 'Edit book')) !!}
                {!! Form::button('<i class="fa fa-minus"></i> Delete', array('id' => 'btn-delete', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => 'Delete book')) !!}
              </div>
              <div id="btn-group-3" class="btn-group toolbar-block" style="display: none">
                {!! Form::button('<i class="fa fa-save"></i> Save', array('id' => 'btn-save', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => 'Save book')) !!}
                {!! Form::button('<i class="fa fa-times"></i> Close', array('id' => 'btn-close', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => 'Return to the grid view (data that has not been saved will be lost.)')) !!}
              </div>
            </div-->

            <div id="container_listData" class="box-body">

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="width:300px">Column</th>
                                <th>Active</th>
                                <th>Mapping</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($model as $col)
                                <tr>
                                    <td>{!! $col->column_name !!}</td>
                                    <td>{!! Form::checkbox('act['.$col->id.']', 1, $col->active, ['id'=>$col->id, 'onchange'=>'updateActive(this, "active")'] ) !!}
                                      <div id="result_error_{{ $col->id }}"></div>
                                    </td>
                                    <td>
                                        {!! Form::text('mapping['.$col->id.']', $col->mapping_column, ['id'=>'txt_' . $col->id, 'onchange'=>'updateActive(this, "mapping_column")'] ) !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- $col->render() --}}
                </div> <!-- table-responsive -->

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
@endsection

<script type="text/javascript">
function updateActive(obj, field)
{
  id = obj.id
  value = obj.value;

  id = id.replace('txt_', '');

  $("#result_error_" + id).html('Saving...');

  if (field == "active") {
    value = 0;
    if($("#" + id).is(':checked')) {
      value = 1;
    }
  }

  var formData = {};
  formData.id = id;
  formData.value = value;
  formData.field = field;

  $.ajax({
	    url : '/lib-column-update/',
	    type: "GET",
	    data : formData,
	    success: function(data, textStatus, jqXHR)
	    {
    			  $("#result_error_" + id).html(data);
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    }
	});
}
</script>
