@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12'>
			<div class="box-body">

                {!! Form::open( ['route' => 'import.excel', 'id' => 'form_import_excel', 'files' => true] ) !!}
                
                <p>Select file to upload:</p>

                {!! Form::file('importExcel'); !!}

                <br>
                <div class="form-group">
                    {!! Form::submit( 'Upload', ['class' => 'btn btn-primary', 'id' => 'btn-submit-edit'] ) !!}
                    <a href="{!! route('dashboard') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                </div>

                {!! Form::close() !!}
            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection
