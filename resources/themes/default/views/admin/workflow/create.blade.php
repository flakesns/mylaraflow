@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <div class="box-body">

                {!! Form::open( ['route' => 'admin.workflow.createPost', 'id' => 'form_create'] ) !!}

                    <div class="nav-tabs-custom">
                        <div class="tab-content">

                            <div class="tab-pane active" id="tab_details">

                                    <div class="form-group">
                                        {!! Form::label('Category') !!}
                                        {!! Form::text('category_name', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('Description') !!}
                                        {!! Form::text('category_desc', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('Lead/Actionee') !!}
                                        {!! Form::select( 'lead_actionee', $users, null, ['class' => 'js-parents form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('1st Approval') !!}
                                        {!! Form::select( "approval_1", $users, null, ['class' => 'js-parents form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('2nd Approval') !!}
                                        {!! Form::select( "approval_2", $users, null, ['class' => 'js-parents form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('3rd Approval') !!}
                                        {!! Form::select( "approval_3", $users, null, ['class' => 'js-parents form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-plus"></i> Save
                                        </button>
                                        <a href="{!! route('admin.workflow.index') !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                                    </div>

                             </div>
                          </div>
                    </div>

                {!! Form::close() !!}

            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@section('body_bottom')
    <script src="{{ asset ("/bower_components/admin-lte/select2/js/select2.min.js") }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-parents").select2();
        });
    </script>

@endsection
