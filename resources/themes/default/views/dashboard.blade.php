@extends('layouts.master')

@section('head_extra')
    <!-- jVectorMap 1.2.2 -->
    <link href="{{ asset("/bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="row">
<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-aqua">
    <div class="inner">
      <h3>{{ $COUNT_CLOSE_CASE }}</h3>
      <p>Close Case</p>
    </div>
    <div class="icon">
      <i class="ion ion-bag"></i>
    </div>
    <a href="{{ route('lib-data.closeCase') }}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div><!-- ./col -->
<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-green">
    <div class="inner">
      <h3>{{ $COUNT_ON_GOING }}</h3>
      <p>Ongoing Process</p>
    </div>
    <div class="icon">
      <i class="ion ion-stats-bars"></i>
    </div>
    <a href="{{ route('lib-data.pendingForApproval') }}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div><!-- ./col -->
<div class="col-lg-3 col-xs-6">
  <!-- small box -->
  <div class="small-box bg-yellow">
    <div class="inner">
      <h3>{{ $COUNT_PENDING_RESPONSE }}</h3>
      <p>Pending for Response</p>
    </div>
    <div class="icon">
      <i class="ion ion-person-add"></i>
    </div>
    <a href="{{ route('lib-data.pendingForResponse') }}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div><!-- ./col -->

@if ($SHOW_THIS)
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $COUNT_UNASSIGN_ACTION }}</h3>
          <p>Unassign Items</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="{{ route('lib-data.listData') }}" class="small-box-footer">More <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
@endif
</div><!-- /.row -->


<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title">Case Percentage</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-8">
          <div class="chart-responsive">
            <canvas id="pieChart" height="150"></canvas>
          </div><!-- ./chart-responsive -->
        </div><!-- /.col -->
        <div class="col-md-4">
          <ul class="chart-legend clearfix">
            @if ($SHOW_THIS)
                <li><i class="fa fa-circle-o text-red"></i> Unassign Items</li>
            @endif
            <li><i class="fa fa-circle-o text-yellow"></i> Pending for Response</li>
            <li><i class="fa fa-circle-o text-green"></i> Ongoing Process</li>
            <li><i class="fa fa-circle-o text-aqua"></i> Close Case</li>
          </ul>
        </div><!-- /.col -->
      </div><!-- /.row -->
        </div><!-- /.box-body -->
</div><!-- /.box -->

    <div class='row'>
        <div class='col-md-6'>
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Pending Response</h3>
                    <div class="box-tools pull-right">
                        <span class="label label-warning">Pending</span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
               <div class="box-body" style="padding-left: 10px">
                  @foreach ($LIST_PENDING_RESPONSE as $obj)
                        <a href="{{ route('lib-data.edit', ['id'=>$obj->id]) }}">ActionNo {{ $obj->ActionNo }}</a><br>
                  @endforeach
                  <a href="{{ route('lib-data.pendingForResponse') }}">more..</a><br>
                </div><!-- /.box-body -->
            </div><!-- /.box -->


            <!-- PROJECT STATUS -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Pending For Approval / On-going process</h3>
                    <div class="box-tools pull-right">
                        <span class="label label-success">On-going</span>
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body" style="padding-left: 10px">
                  @foreach ($LIST_ON_GOING as $obj)
                        <a href="{{ route('lib-data.viewPending', ['id'=>$obj->id]) }}">ActionNo {{ $obj->ActionNo }}</a><br>
                  @endforeach
                  <a href="{{ route('lib-data.pendingForApproval') }}">more..</a><br>
                </div><!-- /.box-body -->
                <div class="box-footer">

                </div><!-- /.box-footer-->
            </div><!-- /.box -->

        </div><!-- /.col -->
        <div class='col-md-6'>
            <!-- USERS LIST -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Rejected ActionNo.</h3>
                    <div class="box-tools pull-right">
                        <span class="label label-danger">Reject</span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body" style="padding-left: 10px">
                  @foreach ($LIST_REJECT as $obj)
                        <a href="{{ route('lib-data.viewPending', ['id'=>$obj->id]) }}">ActionNo {{ $obj->ActionNo }}</a><br>
                  @endforeach
                  <a href="{{ route('lib-data.pendingForApproval') }}">more..</a><br>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">

                </div><!-- /.box-footer -->
            </div>

            <!-- BROWSER USAGE -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Close Case</h3>
                    <div class="box-tools pull-right">
                        <span class="label label-primary">Close Case</span>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
               <div class="box-body" style="padding-left: 10px">
                  @foreach ($LIST_CLOSE_CASE as $obj)
                        <a href="{{ route('lib-data.viewPending', ['id'=>$obj->id]) }}">ActionNo {{ $obj->ActionNo }}</a><br>
                  @endforeach
                  <a href="{{ route('lib-data.closeCase') }}">more..</a><br>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">

                </div><!-- /.box-footer -->
            </div>
        </div><!-- /.col -->

    </div><!-- /.row -->
    

    <script src="{{ asset ("/bower_components/admin-lte/plugins/chartjs/Chart.min.js") }}"></script>
    <script>
//-------------
//- PIE CHART -
//-------------
// Get context with jQuery - using jQuery's .get() method.
var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
var pieChart = new Chart(pieChartCanvas);
var PieData = [
  {
    value: {{ $COUNT_UNASSIGN_ACTION }},
    color: "#f56954",
    highlight: "#f56954",
    label: "Unassign Items"
  },
  {
    value: {{ $COUNT_ON_GOING }},
    color: "#00a65a",
    highlight: "#00a65a",
    label: "Ongoing Process"
  },
  {
    value: {{ $COUNT_PENDING_RESPONSE }},
    color: "#f39c12",
    highlight: "#f39c12",
    label: "Pending Response"
  },
  {
    value: {{ $COUNT_CLOSE_CASE }},
    color: "#00c0ef",
    highlight: "#00c0ef",
    label: "Close Case"
  }
];
var pieOptions = {
  //Boolean - Whether we should show a stroke on each segment
  segmentShowStroke: true,
  //String - The colour of each segment stroke
  segmentStrokeColor: "#fff",
  //Number - The width of each segment stroke
  segmentStrokeWidth: 1,
  //Number - The percentage of the chart that we cut out of the middle
  percentageInnerCutout: 50, // This is 0 for Pie charts
  //Number - Amount of animation steps
  animationSteps: 100,
  //String - Animation easing effect
  animationEasing: "easeOutBounce",
  //Boolean - Whether we animate the rotation of the Doughnut
  animateRotate: true,
  //Boolean - Whether we animate scaling the Doughnut from the centre
  animateScale: false,
  //Boolean - whether to make the chart responsive to window resizing
  responsive: true,
  // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  maintainAspectRatio: false,
  //String - A legend template
  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
  //String - A tooltip template
  tooltipTemplate: "<%=value %> <%=label%> users"
};
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
pieChart.Doughnut(PieData, pieOptions);
//-----------------
//- END PIE CHART -
//-----------------
</script>
@endsection


