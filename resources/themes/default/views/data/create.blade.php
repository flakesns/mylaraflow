@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    @include('partials._head_extra_select2_css')
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <div class="box-body">

              <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_details" data-toggle="tab" aria-expanded="true">Details</a></li>
                  </ul>
                  <div class="tab-content">

                      <div class="tab-pane active" id="tab_details">
                        {!! Form::open( ['route' => 'lib-data.createPost', 'id' => 'form_create'] ) !!}
                            @include('partials._lib_data_form', ['display'=>'edit'])
                        {!! Form::close() !!}
                     </div>

                    </div><!-- /.tab-content -->
                </div><!-- /.nav-tabs-custom-->

            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@section('body_bottom')
    <!-- Select2 js -->
    @include('partials._body_bottom_select2_js_role_search')
    <!-- form submit -->
    @include('partials._body_bottom_submit_user_edit_form_js')
@endsection
