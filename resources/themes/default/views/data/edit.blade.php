<?php
$display_actionee = '';
if (!Auth::user()->can('assign-actionee')) {
  $display_actionee = 'display:none';
}
?>


@extends('layouts.master')

@section('head_extra')
    <!-- Select2 css -->
    {{--@include('partials._head_extra_select2_css')--}}
    <link href="{{ asset ("/bower_components/admin-lte/plugins/datepicker/datepicker3.css") }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset ("/bower_components/admin-lte/plugins/datepicker/bootstrap-datepicker.js") }}"></script>

    <script type="text/javascript">
    $(function() {
        $('#response_due_date').datepicker({
            format: 'dd/mm/yyyy',
        });

        <?php if (isset($tab) && $tab != ''): ?>
              $('.nav-tabs a[href="#tab_<?php echo $tab ?>"]').tab('show');
        <?php endif ?>
    });


    </script>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <div class="box-body">

              <div class="nav-tabs-custom">
                  <ul id="tabs" class="nav nav-tabs">
                      <li class="active"><a href="#tab_details" data-toggle="tab" aria-expanded="true">Details</a></li>
                      @if (!Auth::user()->hasRole('coordinator'))
                          <li class=""><a href="#tab_response" data-toggle="tab" aria-expanded="true">Response</a></li>
                          <li class=""><a href="#tab_delegate" data-toggle="tab" aria-expanded="true">Delegate</a></li>
                      @endif
                      <li class="" style="{{ $display_actionee }}"><a href="#tab_actionee" data-toggle="tab" aria-expanded="false">Actionee</a></li>
                      <li class=""><a href="#tab_status" data-toggle="tab" aria-expanded="false">Status</a></li>
                  </ul>
                  <div class="tab-content">

                          <div class="tab-pane active" id="tab_details">
                            {!! Form::model( $model, ['route' => ['lib-data.update', $model->id], 'method' => 'PATCH', 'id' => 'form_edit_data',  'files'=>true] ) !!}
                                @include('partials._lib_data_form', ['display'=>'edit'])
                            {!! Form::close() !!}
                          </div>

                          <div class="tab-pane" id="tab_response">
                            {!! Form::model( $model, ['route' => ['lib-data.updateResponse', $model->id], 'method' => 'POST', 'id' => 'form_edit_data',  'files'=>true] ) !!}
                              @include('partials._lib_data_form_response')
                            {!! Form::close() !!}
                          </div>

                          @if (!Auth::user()->hasRole('coordinator'))
                            <div class="tab-pane" id="tab_delegate">
                              {!! Form::model( $model, ['route' => ['lib-data.updateActionee', $model->id], 'method' => 'PATCH', 'id' => 'form_data_delegate',  'files'=>true] ) !!}
                                  @include('partials._lib_data_form_delegate')
                              {!! Form::close() !!}
                            </div>
                          @endif

                          <div class="tab-pane" id="tab_actionee">
                            {!! Form::model( $model, ['route' => ['lib-data.updateActionee', $model->id], 'method' => 'PATCH', 'id' => 'form_data_actionee',  'files'=>true] ) !!}
                                @include('partials._lib_data_form_actionee')
                            {!! Form::close() !!}
                          </div>

                          <div class="tab-pane" id="tab_status">
                            {!! Form::model( $model, ['route' => ['lib-data.update', $model->id], 'method' => 'PATCH', 'id' => 'form_edit_data',  'files'=>true] ) !!}
                              @include('partials._lib_data_form_status')
                            {!! Form::close() !!}
                          </div>

                    </div><!-- /.tab-content -->
                </div><!-- /.nav-tabs-custom-->

            </div><!-- /.box-body -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection
