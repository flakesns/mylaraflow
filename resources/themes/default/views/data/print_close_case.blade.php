<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ config('app.short_name') }} | {{ $page_title or "Page Title" }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Set a meta reference to the CSRF token for use in AJAX request -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons 4.4.0 -->
    <link href="{{ asset("/bower_components/admin-lte/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.1 -->
    <link href="{{ asset("/bower_components/admin-lte/ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />

    <!-- Application CSS-->
    <link href="{{ asset(elixir('css/all.css')) }}" rel="stylesheet" type="text/css" />

    <!-- Head -->
    @include('partials._head')

      <!-- REQUIRED JS SCRIPTS -->

      <!-- jQuery 2.1.4 -->
      <script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
      <!-- Bootstrap 3.3.2 JS -->
      <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
      <!-- AdminLTE App -->
      <script src="{{ asset ("/bower_components/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>

      <!-- Optionally, you can add Slimscroll and FastClick plugins.
            Both of these plugins are recommended to enhance the
            user experience. Slimscroll is required when using the
            fixed layout. -->

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <!-- Application JS-->
      <script src="{{ asset(elixir('js/all.js')) }}"></script>

      <!-- Optional header section  -->
      @yield('head_extra')

  </head>

<body>

<?php
$td_head_style = "font-weight:bold; color:#fff; background-color:#8C1717";
$table_style = "padding-bottom: 15px";
?>

<table width="100%">
  <tr><th style="text-align:center">North Malay Basin Bergading CPP & WHP EPCIC Project</th></tr>
  <tr><th style="text-align:center">EHS Action Sheet</th></tr>
</table>

<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>">EHS Action Details</td></tr>
  <tr><td>
        {{--
        ActionNo: {{ $arr['ActionNo'] }}
        StudyActionNo: {{ $arr['StudyActionNo'] }}
        Rev: {{ $arr['Rev.'] }}
        Location: {{ $arr['Location'] }}
        DocumentNo: {{ $arr['DocumentNo'] }}
        --}}

        @foreach($arrPre as $key => $value)
            {{ $key }}: {{ $value }} &nbsp;&nbsp;
        @endforeach

      </td>
  </tr>
</table>

@if (isset($arr['Cause']))
<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>">EHS Action Cause</td></tr>
  <tr><td>{{ $arr['Cause'] }}</td></tr>
</table>
@endif

@if (isset($arr['Consequence']))
<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>">EHS Action Consequence</td></tr>
  <tr><td>{{ $arr['Consequence'] }}</td></tr>
</table>
@endif

@if (isset($arr['Recommendation']))
<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>">EHS Action Recommendation</td></tr>
  <tr><td>{{ $arr['Recommendation'] }}</td></tr>
</table>
@endif

<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?> ;width:70%;">EHS Action Response</td><td style="text-align:right">{{ $arr['response_user'] }}</td></tr>
  <tr><td colsan="2">Rev. {{ $arr['response_rev' ]}} : {{ $arr['response_text'] }}</td></tr>
</table>

<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?> ;width:70%;">EHS Action Response Rejected</td>
      <td style="text-align:right"><?php if (isset($arr['reject_comment'])) { echo $arr['reject_user']; } ?></td>
  </tr>
  <tr>
      <td colsan="2">
        <?php if (isset($arr['reject_comment'])) { echo $arr['reject_comment']; } ?>
      </td>
  </tr>
</table>

<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>" colspan="2">EHS Action Response Approved</td></tr>
  <?php
      if (isset($arrApprove)) {
        foreach ($arrApprove as $key => $arrAppr) {
            echo "<tr><td style='width:70%;'>{$arrAppr['approver_user']} <br> Remarks: <i>{$arrAppr['approver_comment']}</i> <br><br></td>
                  <td style='text-align:right'>{$arrAppr['approver_date']}</td></tr>";
        }
      }
  ?>
</table>

<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>">Future Action Required</td></tr>

</table>

<table width="100%" style="<?php echo $table_style ?>">
  <tr><td style="<?php echo $td_head_style ?>" colspan="2">EHS Action Closed-Out</td></tr>
   <tr><td>ClosedBy: {{ $arr['close_by'] }}</td>
      <td style='text-align:right'>ClosedDate: {{ $arr['close_date'] }}</td>
    </tr>
</table>
<table width="100%">
  <tr><td>North Malay Basin Bergading CPP & WHP EPCIC Project EHS Action Sheet</td></tr>
</table>

{{--
<div class="row">
        @foreach ($model->data_column as $temp)
            <div class="col-md-3">
                <dl>
                      <dt>{{ $arrColumnHeader[$temp->lib_column_name_id] }}</dt>
                      <dd>{{ $temp->data_value }}</dd>
                </dl>


            </div>
        @endforeach
</div>
--}}
</body>
</html>
