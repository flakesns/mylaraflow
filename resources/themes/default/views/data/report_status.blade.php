<?php

?>

@extends('layouts.master')

@section('head_extra')

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
                <div class="box-header with-border">

                    <div id="btn-toolbar" class="section-header btn-toolbar pull-right" role="toolbar">

                        <div id="btn-group-1" class="btn-group">

                          <div class="btn-group">
                             {!! Form::button('<i class="fa fa-share-square-o"></i> Export <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
                             <ul class="dropdown-menu">
                               <li><a href="{{ route('lib-data.getFile', 'action_status.xlsx') }}" id='export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xlsx</a></li>
                               <li><a id='export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
                             </ul>
                          </div>
                        </div>
                        {!! Form::button('<i class="fa fa-print"></i> Print', array('id' => 'btn-edit', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => 'Print')) !!}
                    </div>
                </div>


        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
        <div class="box box-primary">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                      <tr>
                          <th style="width:120px">Project</th>
                          <th style="width:100px;">ActionNo</th>
                          <th style="width:100px">Revision</th>
                          <th style="width:150px">Date Assigned</th>
                          <th style="width:150px">Response Due Date</th>
                          <th>Current Status</th>
                          <th>Lead/Actionee</th>
                          <th>1st Approval</th>
                          <th>2nd Approval</th>
                          <th>3rd Approval</th>

                      </tr>
                    </thead>

                    <thead>
                      <tr>
                          <th><i>Filter</i></th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::select('A', array('','Approve', 'Reject', 'On Going'), ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>

                      </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1123</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1422</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3101</a></td>
                            <td>2</td>
                            <td>15/08/2016</th>
                            <td>04/09/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3104</a></td>
                            <td>1</td>
                            <td>16/08/2016</th>
                            <td>06/09/2016</td>
                            <td><span class="label label-danger">Reject</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td></td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1334</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-warning">On Going</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1422</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3101</a></td>
                            <td>2</td>
                            <td>15/08/2016</th>
                            <td>04/09/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3104</a></td>
                            <td>1</td>
                            <td>16/08/2016</th>
                            <td>06/09/2016</td>
                            <td><span class="label label-danger">Reject</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td></td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3422</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-warning">On Going</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">4321</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">6433</a></td>
                            <td>1</td>
                            <td>16/08/2016</th>
                            <td>06/09/2016</td>
                            <td><span class="label label-danger">Reject</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td></td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">2987</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-warning">On Going</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">5431</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1112</a></td>
                            <td>2</td>
                            <td>15/08/2016</th>
                            <td>04/09/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">5391</a></td>
                            <td>1</td>
                            <td>16/08/2016</th>
                            <td>06/09/2016</td>
                            <td><span class="label label-danger">Reject</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td></td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">7487</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-warning">On Going</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3101</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3453</a></td>
                            <td>1</td>
                            <td>16/08/2016</th>
                            <td>06/09/2016</td>
                            <td><span class="label label-danger">Reject</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td></td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">6491</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-warning">On Going</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">2269</a></td>
                            <td>1</td>
                            <td>10/08/2016</th>
                            <td>30/08/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">2147</a></td>
                            <td>2</td>
                            <td>15/08/2016</th>
                            <td>04/09/2016</td>
                            <td><span class="label label-success">Approve</span></td>
                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
    </div>
</div><!-- /.row -->

@endsection
