@extends('layouts.master')


<!-- Optional bottom section for modals etc... -->
@section('head_extra')
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("/jquery-ui/base/jquery-ui.min.css") }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("/jqgrid/css/ui.jqgrid.css") }}"/>

    <script type="text/javascript" src="{{ asset ("/jquery-ui/base/jquery-ui.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset ("/jqgrid/js/i18n/grid.locale-en.js") }}"></script>
    <script type="text/javascript" src="{{ asset ("/jqgrid/js/jquery.jqGrid.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset ("/jqgrid/helper/helpers.js") }}"></script>

    <script type="text/javascript">
    $(document).ready(function()
    		{
    		  //Attach Bootstrap tooltips to all toolbar buttons
    		  $('.tutorial-tooltip').tooltip();


          $('#print').click(function()
    		  {

    			  rowid = $('#listData').jqGrid('getGridParam', 'selrow');
    			  rowdata = $('#listData').getRowData(rowid);
    			  var id = $('#listData').getCell($('#listData').getGridParam("selrow"), 'id');
    			  if (typeof(id) == "undefined") {
  				    alert('Please select item');
  				    return;
  				  }
    			  location.href = "<?php echo url('print-close-case'); ?>/" + id + "/0/pending_for_approval";

    		  });

    		  $('#print_attach').click(function()
    		  {

    			  rowid = $('#listData').jqGrid('getGridParam', 'selrow');
    			  rowdata = $('#listData').getRowData(rowid);
    			  var id = $('#listData').getCell($('#listData').getGridParam("selrow"), 'id');
    			  if (typeof(id) == "undefined") {
    				    alert('Please select item');
    				    return;
    			  }

    			  location.href = "<?php echo url('print-close-case'); ?>/" + id + "/attach/pending_for_approval";


    		  });

    		  $('#export-all').click(function()
    		  {
    			  location.href = "<?php echo url('export-excel'); ?>/";
    		  });

    		  $('#export-selected').click(function()
    		  {
        		  alert(global_id_arr)
    			  location.href = "<?php echo url('export-excel'); ?>/";
    		  });




    		  //Adding form validation usign the jQuery MG Validation Plugin.
    		  //$('#book-form').jqMgVal('addFormFieldsValidations', {'helpMessageClass':'col-sm-10'});

    		  //Binds onClick event to the "New" button.
    		  $('#btn-new').click(function()
    		  {

        		  location.href = "<?php echo url('lib-data/create'); ?>";

    		    //Disables all buttons within the toolbar.
    		    //The "disabledButtonGroup" is a custom helper function, its definition
    		    //can be foound in the public/assets/tutorial/js/helpers.js script.
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the third button group (save and close).
    		    //The "enabledButtonGroup" is a custom helper function, its definition
    		    //can be foound in the public/assets/tutorial/js/helpers.js script.
    		    $('#btn-group-3').enableButtonGroup();
    		    //Shows the form title.
    		    $('#form-new-title').removeClass('hidden');
    		    //Manually hide the tooltips (fix for firefox).
    		    $('.tooltip').tooltip('hide');
    		    //This is a bootstrap javascript effect to hide the grid.
    		    $('#grid-section').collapse('hide');
    		  });

    		  //Binds onClick event to the "Refresh" button.
    		  $('#btn-refresh').click(function()
    		  {
    		    //When toolbar is enabled, this method should be use to clean the toolbar and refresh the grid.
    		    $('#listData')[0].clearToolbar();
    		    //Disables all buttons within the toolbar
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the first button group (new, refresh and export)
    		    $('#btn-group-1').enableButtonGroup();
    		  });

    		  //Binds onClick event to the "xls" button.
    		  $('#export-xls').click(function()
    		  {
    			  location.href = "<?php echo url('lib-data/export_to_excel/'); ?>/" + id;
    		    //Triggers the grid XLS export functionality.
    		    $('#listDataXlsButton').click();
    		  });

    		  //Binds onClick event to the "csv" button.
    		  $('#export-csv').click(function()
    		  {
    		    //Triggers the grid CSV export functionality.
    		    $('#listDataCsvButton').click();
    		  });

    		  //Bind onClick event to the "Edit" button.
    		  $('#btn-edit').click(function()
    		  {
    			  rowid = $('#listData').jqGrid('getGridParam', 'selrow');
    			  rowdata = $('#listData').getRowData(rowid);
    			  var id = $('#listData').getCell($('#listData').getGridParam("selrow"), 'id');
    			  location.href = "<?php echo url('lib-data/view_pending/'); ?>/" + id;

    		    //Gets the selected row id.
    		    rowid = $('#BookGrid').jqGrid('getGridParam', 'selrow');
    		    //Gets an object with the selected row data.
    		    rowdata = $('#BookGrid').getRowData(rowid);
    		    //Fills out the form with the selected row data (the id of the
    		    //object must match the id of the form elements).
    		    //This is a custom helper function, its definition
    		    //can be foound in the public/assets/tutorial/js/helpers.js script.
    		    populateFormFields(rowdata, '');
    		    //Disables all buttons within the toolbar.
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the third button group (save and close).
    		    $('#btn-group-3').enableButtonGroup();
    		    //Shows the form title.
    		    $('#form-edit-title').removeClass('hidden');
    		    //Manually hide the tooltips (fix for firefox).
    		    $('.tooltip').tooltip('hide');
    		    //This is a bootstrap javascript effect to hide the grid.
    		    $('#grid-section').collapse('hide');
    		  });

    		  //Bind onClick event to the "Delete" button.
    		  $('#btn-delete').click(function()
    		  {
    		    //Gets the selected row id
    		    rowid = $('#BookGrid').jqGrid('getGridParam', 'selrow');
    		    //Gets an object with the selected row data
    		    rowdata = $('#BookGrid').getRowData(rowid);

    		    //Sends an Ajax request to the server.
    		    $.ajax(
    		    {
    		      type: 'POST',
    		      data: JSON.stringify({'id':rowdata['id']}),
    		      dataType : 'json',
    		      url: $('#book-form').attr('action') + '/delete',
    		      error: function (jqXHR, textStatus, errorThrown)
    		      {
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		        alert('Something went wrong, please try again later.');
    		      },
    		      beforeSend:function()
    		      {
    		        $('#app-loader').removeClass('hidden');
    		        $('#main-panel-fieldset').attr('disabled','disabled');
    		      },
    		      success:function(json)
    		      {
    		        if(json.success)
    		        {
    		          //Shows a message after an element.
    		          //This is a custom helper function, its definition
    		          //can be foound in the public/assets/tutorial/js/helpers.js script.
    		          $('#btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.message, 5000);
    		        }
    		        else
    		        {
    		          $('#btn-toolbar').showAlertAfterElement('alert-danger alert-custom', json.message, 5000);
    		        }

    		        //Triggers the "Refresh" button funcionality.
    		        $('#btn-refresh').click();
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		      }
    		    });

    		  });

    		  //Bind onClick event to the "Save" button.
    		  $('#btn-save').click(function()
    		  {
    		    var url = $('#book-form').attr('action');

    		    //Check is the form is valid usign the jQuery MG Validation Plugin.
    		    if(!$('#book-form').jqMgVal('isFormValid'))
    		    {
    		      return;
    		    }

    		    if($('#id').isEmpty())
    		    {
    		      url += '/new';
    		    }
    		    else
    		    {
    		      url += '/edit';
    		    }

    		    //Send an Ajax request to the server.
    		    $.ajax(
    		    {
    		      type: 'POST',
    		      //Creates an object from form fields.
    		      //The "formToObject" is a custom helper function, its definition
    		      //can be foound in the public/assets/tutorial/js/helpers.js script.
    		      data: JSON.stringify($('#book-form').formToObject('')),
    		      dataType : 'json',
    		      url: url,
    		      error: function (jqXHR, textStatus, errorThrown)
    		      {
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		        alert('Something went wrong, please try again later.');
    		      },
    		      beforeSend:function()
    		      {
    		        $('#app-loader').removeClass('hidden');
    		        $('#main-panel-fieldset').attr('disabled','disabled');
    		      },
    		      success:function(json)
    		      {
    		        if(json.success)
    		        {
    		          $('#btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.message, 5000);
    		        }
    		        else
    		        {
    		          $('#btn-toolbar').showAlertAfterElement('alert-danger alert-custom', json.message, 5000);
    		        }

    		        //Triggers the "Close" button funcionality.
    		        $('#btn-close').click();
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		      }
    		    });
    		  });

    		  //Bind onClick event to the "Close" button.
    		  $('#btn-close').click(function()
    		  {
    		    //Disables all buttons within the toolbar.
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the fisrt button group (new, refresh and export).
    		    $('#btn-group-1').enableButtonGroup();
    		    //Hides the form titles
    		    $('#form-new-title').addClass('hidden');
    		    $('#form-edit-title').addClass('hidden');
    		    //Manually hide the tooltips (fix for firefox).
    		    $('.tooltip').tooltip('hide');
    		    //Cleans the form usign the jQuery MG Validation Plugin.
    		    $('#book-form').jqMgVal('clearForm');
    		    //Triggers the "Refresh" button funcionality.
    		    $('#btn-refresh').click();
    		    //This is a bootstrap javascript effect to hide the grid.
    		    $('#form-section').collapse('hide');
    		  });

    		  //This is a bootstrap javascript event that allows you to do
    		  //something when the element is hidden.
    		  $('#grid-section').on('hidden.bs.collapse', function ()
    		  {
    		    //Shows the form.
    		    //This is a bootstrap javascript effect
    		    $('#form-section').collapse('show');
    		    //Focus on the name form field
    		    $('#name').focus();
    		  });

    		  $('#form-section').on('hidden.bs.collapse', function ()
    		  {
    		    //Shows the grid.
    		    $('#grid-section').collapse('show');
    		  });

    		  //Binds focusOut event to the "ASIN" field.
    		  $('#asin').focusout(function()
    		  {
    		    //Focus on the "Save" button.
    		    $('#btn-save').focus();
    		  });
    	});

    </script>
<!--

//-->

@endsection


@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $page_title }}</h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div id="btn-toolbar" class="section-header btn-toolbar" role="toolbar">

                  <div id="btn-group-2" class="btn-group">
                      {!! Form::button('<i class="fa fa-refresh"></i> Refresh', array('id' => 'btn-refresh', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => 'Refresh grid data')) !!}
                      {!! Form::button('<i class="fa fa-edit"></i> View & Response', array('id' => 'btn-edit', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'disabled' => '', 'data-original-title' => 'View')) !!}

                      <div class="btn-group">
                         {!! Form::button('<i class="fa fa-print"></i> Print <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
                         <ul class="dropdown-menu">
                           <li><a id="print"><i class="fa fa-print"></i> Print</a></li>
                           <li><a id="print_attach"><i class="fa fa-print"></i> Print with Attachment</a></li>
                         </ul>
                      </div>

                      <!--div class="btn-group">
                         {!! Form::button('<i class="fa fa-share-square-o"></i> Export <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
                         <ul class="dropdown-menu">
                           <li><a id='export-all'><i class="fa fa-file-text-o"></i> All</a></li>
                           <li><a id='export-selected'><i class="fa fa-file-excel-o"></i> Selected</a></li>
                         </ul>
                      </div-->

                    </div>
                </div>


                <div id="container_listData" class="box-body">
                    <script type="text/javascript">
                  //When a row is clicked the edit and delete button will be enabled.
                    function onSelectRowEvent(rowid, status, e)
                    {
                        //"btn-group-2" is the "ID" of the second button group.
                        //The "enableButtonGroup" is a custom helper function, its definition
                        //can be foound in the public/assets/tutorial/js/helpers.js script.
                        $('#btn-group-2').enableButtonGroup();
                    }
                    </script>
                    {{
                    GridRender::setGridId("listData")
                      ->enableFilterToolbar()
                      //->hideXlsExporter()
                      //->hideCsvExporter()
                      //->setGridOption('url',URL::to('/lib-data/grid-pending-for-approval'))
                      ->setGridOption('url',URL::to('/lib-data/grid-global', array('type_grid'=>$type_grid)))
                      ->setGridOption('width', 'auto')
                      ->setGridOption('height', 'auto')
                      //->setGridOption('viewrecords',true)
                      ->setGridOption('rowNum', 30)
                      ->setGridOption('shrinkToFit',false)
                      //->setGridOption('sortname','last_name')
                      ->setGridOption('caption','Data')
                      ->setGridOption('useColSpanStyle', true)
                      ->setNavigatorOptions('navigator', array('viewtext'=>'view'))
                      ->setNavigatorOptions('view',array('closeOnEscape'=>false))
                      ->setFilterToolbarOptions(array('autosearch'=>true))
                      //->setNavigatorEvent('view', 'beforeShowForm', 'function(){alert("Before show form");}')
                      //->setFilterToolbarEvent('beforeSearch', 'function(){alert("Before search event");}')
                      ->setGridEvent('onSelectRow', 'onSelectRowEvent')
                      ->addColumn(array('label'=>'ID','name'=>'id', 'index'=>'id', 'hidden'=>true))
                      ->addColumn(array('label'=>'ActionNo','name'=>'ActionNo', 'index'=>'ActionNo', 'width'=>55))
                      //->addColumn(array('label'=>'Revision', 'name'=>'revision', 'index'=>'data_response.revision', 'width'=>55))
                      //->addColumn(array('label'=>'StudyActionNo','index'=>'StudyActionNo','width'=>100))
                      //->addColumn(array('label'=>'Study','index'=>'Study','width'=>100))
                      //->addColumn(array('label'=>'Phase','index'=>'Phase','width'=>100))
                      //->addColumn(array('label'=>'Node','index'=>'Node','width'=>100))
                      //->addColumn(array('label'=>'Cause','index'=>'Cause','width'=>100))
                      //->addColumn(array('label'=>'Consequence','index'=>'Consequence','width'=>100))
                      //->addColumn(array('label'=>'SafeGuard','index'=>'SafeGuard','width'=>100))
                      //->addColumn(array('label'=>'Status', 'name'=>'remarks', 'index'=>'data_flow.remarks','width'=>100))
                      ->addColumn(array('label'=>'Lead_Actionee','index'=>'lead_actionee','width'=>100))
                      ->addColumn(array('label'=>'Response Due Date','index'=>'response_due_date','width'=>100))
                      ->renderGrid()
                    }}
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
    @endsection


            <!-- Optional bottom section for modals etc... -->
@section('body_bottom')
    <script language="JavaScript">
        $(window).bind('resize', function() {
            jQuery("#listData").setGridWidth($('#container_listData').width()-10, true);
        }).trigger('resize');


    </script>
@endsection
