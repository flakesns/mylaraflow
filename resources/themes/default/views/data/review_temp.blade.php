@extends('layouts.master')

@section('head_extra')

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
                <div class="box-header with-border">

                    <div id="btn-toolbar" class="section-header btn-toolbar pull-right" role="toolbar">
                        <form method="post" action="{{ route('import.excelReviewTemp') }}">
                        {{ csrf_field() }}
                        <button type="submit" name="save_only" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Save imported data to database">
                            <i class="fa fa-plus"></i> Save Only
                        </button>
                        
                        <button type="submit" name="save_assign"  class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Save, auto assign & send notification to actionee">
                            <i class="fa fa-share-square-o"></i> Save & Notify Actionee
                        </button>
                        
                        <button type="submit" name="cancel" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Cancel & data not saved!">
                            <i class="fa fa-ban"></i> Cancel
                        </button>
                        </form>
                     </div>
                </div>


        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
        <div class="box box-primary">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                      <tr>
                          @foreach ($arrColumnHeader as $col_head)
                              <th>{{ $col_head }}</th>
                          @endforeach
                      </tr>
                    </thead>                    
                    <tbody>
                        @foreach ($model as $obj)
                            <tr>
                                @foreach ($obj->temp_data as $temp)
                                    <td>{{ $temp->data_value }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
    </div>
</div><!-- /.row -->
@endsection