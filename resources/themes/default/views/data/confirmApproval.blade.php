<?php

?>

@extends('layouts.master')

@section('head_extra')

@endsection

@section('content')
{!! Form::model( $model, ['route' => ['lib-data.postApproval', $model->id, $modelResponse->id], 'method' => 'POST', 'id' => 'form_edit_data',  'files'=>true] ) !!}
  {!! Form::hidden('revision_id', $request->revision_id) !!}
  {!! Form::hidden('comments', $request->comments) !!}
  {!! Form::hidden('approver_number', $request->approver_number) !!}
<div class="row">
  <div class="col-md-8">
        <div class="box box-primary">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>

                    </thead>

                    <tbody>
                      <tr>
                          <th>ActionNo</th>
                          <td>{{ $model->ActionNo }}</td>
                      </tr>
                      <tr>
                          <th>Revision</th>
                          <td>{{ $modelResponse->revision }}</td>
                      </tr>
                      <tr>
                          <th>Response</th>
                          <td>{{ $modelResponse->response }}</td>
                      </tr>
                      <tr>
                          <th>Attachment</th>
                          <td>
                            @foreach($modelResponse->attachments as $attach)
                                {!! link_to( route('lib-data.getFile', $attach->filename) . '/' . $attach->file_name, $attach->file_name) !!}
                                <br>
                            @endforeach
                          </td>
                      </tr>

                      <tr>
                          <th>Comments</th>
                          <td>{{ $request->comments }}</td>
                      </tr>
                      <tr>
                          <th>Approval</th>
                          <td>
                            <?php $label = 'success'; ?>
                            @if ($request->approval_status == 'Reject')
                                <?php $label = 'danger'; ?>
                            @endif

                            <span class="label label-{{ $label }}">{{ $request->approval_status }}</span>
                          </td>
                      </tr>
                      <tr>
                          <td></td>
                          <td>
                            <br>
                            <div class="form-group">
                                <p>Are you sure to submit this approval?</p>
                                @if ($request->approval_status == 'Approve')
                                    <button type="submit" name="approval_status" value="Approve" class="btn btn-primary">
                                        <i class="glyphicon glyphicon-ok"></i> Yes, Approve Now
                                    </button>
                                @else
                                    <button type="submit" name="approval_status" value="Reject" class="btn btn-danger">
                                        <i class="glyphicon glyphicon-minus"></i> Yes, Reject Now
                                    </button>
                                @endif

                                <a href="{!! route('lib-data.viewPending', ['id'=>$model->id]) !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                            </div>
                          </td>
                      </tr>
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
    </div>
</div><!-- /.row -->
{!! Form::close() !!}
@endsection
