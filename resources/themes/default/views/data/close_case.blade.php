@extends('layouts.master')


<!-- Optional bottom section for modals etc... -->
@section('head_extra')
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("/jquery-ui/base/jquery-ui.min.css") }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("/jqgrid/css/ui.jqgrid.css") }}"/>

    <script type="text/javascript" src="{{ asset ("/jquery-ui/base/jquery-ui.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset ("/jqgrid/js/i18n/grid.locale-en.js") }}"></script>
    <script type="text/javascript" src="{{ asset ("/jqgrid/js/jquery.jqGrid.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset ("/jqgrid/helper/helpers.js") }}"></script>

    <script type="text/javascript">
    var global_id;
    var global_id_arr = new Array();
    function store_selected(obj)
    {
    	id = obj.id
    	if($("#" + id).is(':checked')) {
        	arr = id.split('_');
    		global_id = arr[1];
    		global_id_arr.push(global_id);
		    value = 1;
		}
    }


    $(document).ready(function()
    		{
    	        $('input:checkbox').removeAttr('checked');
    	        
            	$("#chk_all").click(function(){
            	    $('input:checkbox').not(this).prop('checked', this.checked);

            	    $.each($(".ids:checkbox:checked"), function() {
            	    	store_selected(this);
                    });
            	});
    	
    		  //Attach Bootstrap tooltips to all toolbar buttons
    		  $('.tutorial-tooltip').tooltip();

    		  $('#print').click(function()
    		  {
            /*
    			  rowid = $('#listData').jqGrid('getGridParam', 'selrow');
    			  rowdata = $('#listData').getRowData(rowid);
    			  var id = $('#listData').getCell($('#listData').getGridParam("selrow"), 'id');
    			  if (typeof(id) == "undefined") {
  				    alert('Please select item');
  				    return;
  				  }
    			  location.href = "<?php echo url('print-close-case'); ?>/" + id;
            */
                  id = global_id;
    			  if (typeof(id) == "undefined") {
    				    alert('Please select item');
    				    return;
    			  }
    			  //location.href = "<?php echo url('print-close-case'); ?>/" + id;

    			  var ids = JSON.stringify(global_id_arr);
    			  $('#ids').val(ids);
    			  document.form_print.submit();
    			  
    			  //var ids = JSON.stringify(global_id_arr);
    			  //location.href = "<?php echo url('print-close-case'); ?>/" + ids;
    		  });

    		  $('#print_attach').click(function()
    		  {
    			    /*
    			  rowid = $('#listData').jqGrid('getGridParam', 'selrow');
    			  rowdata = $('#listData').getRowData(rowid);
    			  var id = $('#listData').getCell($('#listData').getGridParam("selrow"), 'id');
    			  if (typeof(id) == "undefined") {
    				    alert('Please select item');
    				    return;
    			  }
    			  */
    			  id = global_id;
    			  if (typeof(id) == "undefined") {
    				    alert('Please select item');
    				    return;
    			  }
    			  //location.href = "<?php echo url('print-close-case'); ?>/" + id + "/attach";

    			  var ids = JSON.stringify(global_id_arr);
    			  $('#ids').val(ids);
    			  $('#attach').val('attach');
    			  document.form_print.submit();

    		  });

    		  $('#export-all').click(function()
    		  {
    			  location.href = "<?php echo url('export-excel'); ?>/";
    		  });

    		  $('#export-selected').click(function()
    		  {
        		  alert(global_id_arr)
    			  location.href = "<?php echo url('export-excel'); ?>/";
    		  });

    		  //Adding form validation usign the jQuery MG Validation Plugin.
    		  //$('#book-form').jqMgVal('addFormFieldsValidations', {'helpMessageClass':'col-sm-10'});

    		  //Binds onClick event to the "New" button.
    		  $('#btn-new').click(function()
    		  {

        		  location.href = "<?php echo url('lib-data/create'); ?>";

    		    //Disables all buttons within the toolbar.
    		    //The "disabledButtonGroup" is a custom helper function, its definition
    		    //can be foound in the public/assets/tutorial/js/helpers.js script.
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the third button group (save and close).
    		    //The "enabledButtonGroup" is a custom helper function, its definition
    		    //can be foound in the public/assets/tutorial/js/helpers.js script.
    		    $('#btn-group-3').enableButtonGroup();
    		    //Shows the form title.
    		    $('#form-new-title').removeClass('hidden');
    		    //Manually hide the tooltips (fix for firefox).
    		    $('.tooltip').tooltip('hide');
    		    //This is a bootstrap javascript effect to hide the grid.
    		    $('#grid-section').collapse('hide');
    		  });

    		  //Binds onClick event to the "Refresh" button.
    		  $('#btn-refresh').click(function()
    		  {
    		    //When toolbar is enabled, this method should be use to clean the toolbar and refresh the grid.
    		    $('#listData')[0].clearToolbar();
    		    //Disables all buttons within the toolbar
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the first button group (new, refresh and export)
    		    $('#btn-group-1').enableButtonGroup();
    		  });

    		  //Binds onClick event to the "xls" button.
    		  $('#export-xls').click(function()
    		  {
    		    //Triggers the grid XLS export functionality.
    		    $('#listDataXlsButton').click();
    		  });

    		  //Binds onClick event to the "csv" button.
    		  $('#export-csv').click(function()
    		  {
    		    //Triggers the grid CSV export functionality.
    		    $('#listDataCsvButton').click();
    		  });

    		  //Bind onClick event to the "Edit" button.
    		  $('#btn-edit').click(function()
    		  {
    			  rowid = $('#listData').jqGrid('getGridParam', 'selrow');
    			  rowdata = $('#listData').getRowData(rowid);
    			  var id = $('#listData').getCell($('#listData').getGridParam("selrow"), 'id');
    			  location.href = "<?php echo url('lib-data/view_pending/'); ?>/" + id;

    		    //Gets the selected row id.
    		    rowid = $('#BookGrid').jqGrid('getGridParam', 'selrow');
    		    //Gets an object with the selected row data.
    		    rowdata = $('#BookGrid').getRowData(rowid);
    		    //Fills out the form with the selected row data (the id of the
    		    //object must match the id of the form elements).
    		    //This is a custom helper function, its definition
    		    //can be foound in the public/assets/tutorial/js/helpers.js script.
    		    populateFormFields(rowdata, '');
    		    //Disables all buttons within the toolbar.
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the third button group (save and close).
    		    $('#btn-group-3').enableButtonGroup();
    		    //Shows the form title.
    		    $('#form-edit-title').removeClass('hidden');
    		    //Manually hide the tooltips (fix for firefox).
    		    $('.tooltip').tooltip('hide');
    		    //This is a bootstrap javascript effect to hide the grid.
    		    $('#grid-section').collapse('hide');
    		  });

    		  //Bind onClick event to the "Delete" button.
    		  $('#btn-delete').click(function()
    		  {
    		    //Gets the selected row id
    		    rowid = $('#BookGrid').jqGrid('getGridParam', 'selrow');
    		    //Gets an object with the selected row data
    		    rowdata = $('#BookGrid').getRowData(rowid);

    		    //Sends an Ajax request to the server.
    		    $.ajax(
    		    {
    		      type: 'POST',
    		      data: JSON.stringify({'id':rowdata['id']}),
    		      dataType : 'json',
    		      url: $('#book-form').attr('action') + '/delete',
    		      error: function (jqXHR, textStatus, errorThrown)
    		      {
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		        alert('Something went wrong, please try again later.');
    		      },
    		      beforeSend:function()
    		      {
    		        $('#app-loader').removeClass('hidden');
    		        $('#main-panel-fieldset').attr('disabled','disabled');
    		      },
    		      success:function(json)
    		      {
    		        if(json.success)
    		        {
    		          //Shows a message after an element.
    		          //This is a custom helper function, its definition
    		          //can be foound in the public/assets/tutorial/js/helpers.js script.
    		          $('#btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.message, 5000);
    		        }
    		        else
    		        {
    		          $('#btn-toolbar').showAlertAfterElement('alert-danger alert-custom', json.message, 5000);
    		        }

    		        //Triggers the "Refresh" button funcionality.
    		        $('#btn-refresh').click();
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		      }
    		    });

    		  });

    		  //Bind onClick event to the "Save" button.
    		  $('#btn-save').click(function()
    		  {
    		    var url = $('#book-form').attr('action');

    		    //Check is the form is valid usign the jQuery MG Validation Plugin.
    		    if(!$('#book-form').jqMgVal('isFormValid'))
    		    {
    		      return;
    		    }

    		    if($('#id').isEmpty())
    		    {
    		      url += '/new';
    		    }
    		    else
    		    {
    		      url += '/edit';
    		    }

    		    //Send an Ajax request to the server.
    		    $.ajax(
    		    {
    		      type: 'POST',
    		      //Creates an object from form fields.
    		      //The "formToObject" is a custom helper function, its definition
    		      //can be foound in the public/assets/tutorial/js/helpers.js script.
    		      data: JSON.stringify($('#book-form').formToObject('')),
    		      dataType : 'json',
    		      url: url,
    		      error: function (jqXHR, textStatus, errorThrown)
    		      {
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		        alert('Something went wrong, please try again later.');
    		      },
    		      beforeSend:function()
    		      {
    		        $('#app-loader').removeClass('hidden');
    		        $('#main-panel-fieldset').attr('disabled','disabled');
    		      },
    		      success:function(json)
    		      {
    		        if(json.success)
    		        {
    		          $('#btn-toolbar').showAlertAfterElement('alert-success alert-custom', json.message, 5000);
    		        }
    		        else
    		        {
    		          $('#btn-toolbar').showAlertAfterElement('alert-danger alert-custom', json.message, 5000);
    		        }

    		        //Triggers the "Close" button funcionality.
    		        $('#btn-close').click();
    		        $('#app-loader').addClass('hidden');
    		        $('#main-panel-fieldset').removeAttr('disabled');
    		      }
    		    });
    		  });

    		  //Bind onClick event to the "Close" button.
    		  $('#btn-close').click(function()
    		  {
    		    //Disables all buttons within the toolbar.
    		    $('#btn-toolbar').disabledButtonGroup();
    		    //Enables the fisrt button group (new, refresh and export).
    		    $('#btn-group-1').enableButtonGroup();
    		    //Hides the form titles
    		    $('#form-new-title').addClass('hidden');
    		    $('#form-edit-title').addClass('hidden');
    		    //Manually hide the tooltips (fix for firefox).
    		    $('.tooltip').tooltip('hide');
    		    //Cleans the form usign the jQuery MG Validation Plugin.
    		    $('#book-form').jqMgVal('clearForm');
    		    //Triggers the "Refresh" button funcionality.
    		    $('#btn-refresh').click();
    		    //This is a bootstrap javascript effect to hide the grid.
    		    $('#form-section').collapse('hide');
    		  });

    		  //This is a bootstrap javascript event that allows you to do
    		  //something when the element is hidden.
    		  $('#grid-section').on('hidden.bs.collapse', function ()
    		  {
    		    //Shows the form.
    		    //This is a bootstrap javascript effect
    		    $('#form-section').collapse('show');
    		    //Focus on the name form field
    		    $('#name').focus();
    		  });

    		  $('#form-section').on('hidden.bs.collapse', function ()
    		  {
    		    //Shows the grid.
    		    $('#grid-section').collapse('show');
    		  });

    		  //Binds focusOut event to the "ASIN" field.
    		  $('#asin').focusout(function()
    		  {
    		    //Focus on the "Save" button.
    		    $('#btn-save').focus();
    		  });
    	});

    </script>
<!--

//-->

@endsection


@section('content')

  <div class="row">
      <div class="col-md-12">
          <div class="box box-default">
                  <div class="box-header with-border">

                      <div id="btn-toolbar" class="section-header btn-toolbar pull-right" role="toolbar">

                        <div id="btn-group-2" class="btn-group">
                          {!! Form::button('<i class="fa fa-refresh"></i> Refresh', array('id' => 'btn-refresh', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => 'Refresh grid data')) !!}

                          <div class="btn-group">
                             {!! Form::button('<i class="fa fa-print"></i> Print <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
                             <ul class="dropdown-menu">
                               <li><a id="print"><i class="fa fa-print"></i> Print</a></li>
                               <li><a id="print_attach"><i class="fa fa-print"></i> Print with Attachment</a></li>
                             </ul>
                          </div>

                          <div class="btn-group">
                             {!! Form::button('<i class="fa fa-share-square-o"></i> Export <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
                             <ul class="dropdown-menu">
                               <li><a id='export-all'><i class="fa fa-file-text-o"></i> All</a></li>
                               <li><a id='export-selected'><i class="fa fa-file-excel-o"></i> Selected</a></li>
                             </ul>
                          </div>
                        </div>

                      </div>
                  </div>


          </div>
      </div>
  </div>

    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">

                <div class="table-responsive">
                    {!! Form::open(array('url' => 'lib-data/close-case')) !!}
                    <table class="table table-hover">
                        <thead>
                          <tr>
                              <th style="width:20px"><input type="checkbox" name="chk_all" id="chk_all" value="1"></th>
                              <th style="">ActionNo</th>
                              <!-- th>Lead/Actionee</th -->
                              <th style="">Close Date</th>
                          </tr>
                        </thead>

                        <thead>
                          <tr>
                              <th><i>Filter</i></th>
                              <th>{!! Form::text('filter["lib_brg_cplx.ActionNo"]', $filters["lib_brg_cplx.ActionNo"], ['class' => 'form-control', 'onchange'=>'filterSearch(this)']); !!}</th>
                              <th>{!! Form::text('filter["data_flow.updated_at"]', null, ['class' => 'form-control', 'onchange'=>'filterSearch(this)']); !!}</th>
                              <!-- th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th-->
                          </tr>
                        </thead>

                        <tbody>
                            <?php
                                foreach ($model as $obj) {
                                    $chk_id = 'chk_' . $obj->lib_brg_cplx_id;
                                    $close_date = date('d-M-Y', strtotime($obj->close_date));
                                    $href = "<a href='".url('lib-data/view_pending/')."/{$obj->lib_brg_cplx_id}'>$obj->ActionNo</a>";
                                    echo "<tr>";
                                    echo "<td><input type='checkbox' id='$chk_id' name='ids[$obj->lib_brg_cplx_id]' value='1' onclick='store_selected(this)' class='ids'></td>";
                                    echo "<td>$href</td>";
                                    echo "<td>$close_date</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                     </table>
                     <input type="submit" name="submitFilter" style="border:0; padding:0; font-size:0;width:0px; height:0px; opacity:0;"/>
                     {!! Form::close() !!}
                 </div> <!-- table-responsive -->
                 
                 <form id="form_print" name="form_print" method="post" action="{{ route('printCloseCase') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="ids" value="">
                    <input type="hidden" name="attach" id="attach" value="">
                 </form>


                <div id="container_listData" class="box-body">
                    <script type="text/javascript">
                  //When a row is clicked the edit and delete button will be enabled.
                    function onSelectRowEvent(rowid, status, e)
                    {
                        //"btn-group-2" is the "ID" of the second button group.
                        //The "enableButtonGroup" is a custom helper function, its definition
                        //can be foound in the public/assets/tutorial/js/helpers.js script.
                        $('#btn-group-2').enableButtonGroup();
                    }

                    function filterSearch(obj)
                    {

                    }

                    </script>
                    {{--
                    GridRender::setGridId("listData")
                      ->enableFilterToolbar()
                      ->hideXlsExporter()
                      ->hideCsvExporter()
                      ->setGridOption('url',URL::to('/lib-data/grid-global/Close_Case'))
                      ->setGridOption('width', 'auto')
                      ->setGridOption('height', 'auto')
                      ->setGridOption('multiselect', true)
                      ->setGridOption('rowNum', 50)
                      ->setGridOption('shrinkToFit',false)
                      //->setGridOption('sortname','last_name')
                      ->setGridOption('caption','Data')
                      //->setGridOption('useColSpanStyle', true)
                      //->setNavigatorOptions('navigator', array('viewtext'=>'view'))
                      //->setNavigatorOptions('view',array('closeOnEscape'=>false))
                      ->setNavigatorOptions('navigator', array('add' => false, 'edit' => false, 'del' => false, 'view' => false, 'refresh' => false))
                      //->setFilterToolbarOptions(array('autosearch'=>true))
                      //->setNavigatorEvent('view', 'beforeShowForm', 'function(){alert("Before show form");}')
                      //->setFilterToolbarEvent('beforeSearch', 'function(){alert("Before search event");}')
                      ->setGridEvent('onSelectRow', 'onSelectRowEvent')
                      ->addColumn(array('label'=>'ID','name'=>'id', 'index'=>'id', 'hidden'=>true, 'width'=>55))
                      ->addColumn(array('label'=>'ActionNo','name'=>'ActionNo', 'index'=>'ActionNo', 'width'=>55))
                      //->addColumn(array('label'=>'Revision', 'index'=>'revision', 'width'=>55))
                      //->addColumn(array('label'=>'StudyActionNo','index'=>'StudyActionNo','width'=>100))
                      //->addColumn(array('label'=>'Study','index'=>'Study','width'=>100))
                      //->addColumn(array('label'=>'Phase','index'=>'Phase','width'=>100))
                      //->addColumn(array('label'=>'Node','index'=>'Node','width'=>100))
                      //->addColumn(array('label'=>'Cause','index'=>'Cause','width'=>100))
                      //->addColumn(array('label'=>'Consequence','index'=>'Consequence','width'=>100))
                      //->addColumn(array('label'=>'SafeGuard','index'=>'SafeGuard','width'=>100))
                      ->renderGrid()
                    --}}
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
    @endsection


            <!-- Optional bottom section for modals etc... -->
@section('body_bottom')
    <script language="JavaScript">
        $(window).bind('resize', function() {
            jQuery("#listData").setGridWidth($('#container_listData').width()-10, true);
        }).trigger('resize');


    </script>
@endsection
