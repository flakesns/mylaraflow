<?php

?>

@extends('layouts.master')

@section('head_extra')

@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
                <div class="box-header with-border">

                    <div id="btn-toolbar" class="section-header btn-toolbar pull-right" role="toolbar">

                        <div id="btn-group-1" class="btn-group">

                          <div class="btn-group">
                             {!! Form::button('<i class="fa fa-share-square-o"></i> Export <span class="caret"></span>', array('class' => 'btn btn-default dropdown-toggle', 'data-container' => 'body', 'data-toggle' => 'dropdown')) !!}
                             <ul class="dropdown-menu">
                               <li><a href="{{ route('lib-data.getFile', 'close_case.xlsx') }}" id='export-xls' class="fake-link"><i class="fa fa-file-excel-o"></i> xlsx</a></li>
                               <li><a id='export-csv' class="fake-link"><i class="fa fa-file-text-o"></i> csv</a></li>
                             </ul>
                          </div>
                        </div>
                        {!! Form::button('<i class="fa fa-print"></i> Print', array('id' => 'btn-edit', 'class' => 'btn btn-default tutorial-tooltip', 'data-container' => 'body', 'data-toggle' => 'tooltip', 'data-original-title' => 'Print')) !!}
                    </div>
                </div>


        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
        <div class="box box-primary">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                      <tr>
                          <th style="width:120px">Project</th>
                          <th style="width:100px;">ActionNo</th>
                          <th style="width:100px">Revision</th>
                          <th>Lead/Actionee</th>
                          <th>1st Approval</th>
                          <th>2nd Approval</th>
                          <th>3rd Approval</th>
                          <th style="width:150px">Close Date</th>
                          <th>Status</th>
                          <th>Digital Sign</th>
                      </tr>
                    </thead>

                    <thead>
                      <tr>
                          <th><i>Filter</i></th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th>{!! Form::text('A', null, ['class' => 'form-control']); !!}</th>
                          <th></th>
                          <th></th>
                      </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3101</a></td>
                            <td>1</td>

                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td></td>
                            <td>10/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1422</a></td>
                            <td>1</td>

                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>
                            <td>10/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>
                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">6422</a></td>
                            <td>2</td>

                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>
                            <td>15/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">2324</a></td>
                            <td>1</td>

                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td></td>
                            <td>10/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">1732</a></td>
                            <td>1</td>

                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>
                            <td>10/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>
                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">8222</a></td>
                            <td>2</td>

                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>
                            <td>15/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">4217</a></td>
                            <td>1</td>

                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td></td>
                            <td>10/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>

                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">4855</a></td>
                            <td>1</td>

                            <td>Hafiz</td>
                            <td>Blend</td>
                            <td>Walter</td>
                            <td>-</td>
                            <td>10/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>
                        </tr>
                        <tr>
                            <td>Lara Project</td>
                            <td><a href="{{ url('lib-data/view_pending', ['id'=>22]) }}">3773</a></td>
                            <td>2</td>

                            <td>Celine</td>
                            <td>Blend</td>
                            <td>-</td>
                            <td>-</td>
                            <td>15/08/2016</th>
                            <td><span class="label label-success">Approve</span></td>
                            <td><img src="{{url('/images/correct.png')}}" alt="Image"/> <i>Valid</i></td>

                        </tr>
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
    </div>
</div><!-- /.row -->

@endsection
