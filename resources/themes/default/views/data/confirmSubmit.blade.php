<?php

?>

@extends('layouts.master')

@section('head_extra')

@endsection

@section('content')
{!! Form::model( $model, ['route' => ['lib-data.submitApproval', $model->id, $modelResponse->id], 'method' => 'POST', 'id' => 'form_edit_data',  'files'=>true] ) !!}
  {!! Form::hidden('action', 'Submit For Approval') !!}
<div class="row">
  <div class="col-md-8">
        <div class="box box-primary">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>

                    </thead>

                    <tbody>
                      <tr>
                          <th>ActionNo</th>
                          <td>{{ $model->ActionNo }}</td>
                      </tr>
                      <tr>
                          <th>Revision</th>
                          <td>{{ $modelResponse->revision }}</td>
                      </tr>
                      <tr>
                          <th>Response</th>
                          <td>{{ $modelResponse->response }}</td>
                      </tr>
                      <tr>
                          <th>Attachment</th>
                          <td>
                            @foreach($modelResponse->attachments as $attach)
                                {!! link_to( route('lib-data.getFile', $attach->filename) . '/' . $attach->file_name, $attach->file_name) !!}
                                <br>
                            @endforeach
                          </td>
                      </tr>
                      <tr>
                          <td></td>
                          <td>
                            <br>
                            <div class="form-group">
                                <p>Are you sure to submit this ActionNo for approval?</p>
                                <button type="submit" class="btn btn-primary">
                                    Yes, Submit for Approval
                                </button>
                                <a href="{!! route('lib-data.edit', ['id'=>$model->id]) !!}" title="{{ trans('general.button.cancel') }}" class='btn btn-default'>{{ trans('general.button.cancel') }}</a>
                            </div>
                          </td>
                      </tr>
                    </tbody>
                </table>
              </div> <!-- table-responsive -->
        </div>
    </div>
</div><!-- /.row -->
{!! Form::close() !!}
@endsection
