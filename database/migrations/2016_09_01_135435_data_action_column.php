<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataActionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_column', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lib_brg_cplx_id');
            $table->integer('lib_column_name_id');
            $table->text('data_value')->nullable();
            $table->boolean('active')->default(1);
            $table->unique(['lib_brg_cplx_id', 'lib_column_name_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_column');
    }
}
