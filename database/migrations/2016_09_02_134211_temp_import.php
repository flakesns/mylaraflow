<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TempImport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_action_no', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('user_id');
            $table->text('ActionNo');
            $table->timestamps();
        });
        
            Schema::create('t_data_column', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('t_action_no_id')->unsigned();
                $table->integer('lib_column_name_id');
                $table->text('data_value')->nullable();
                $table->unique(['t_action_no_id', 'lib_column_name_id']);
                $table->timestamps();
                $table->foreign('t_action_no_id')->references('id')->on('t_action_no')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_data_column');
        Schema::drop('t_action_no');
    }
}
