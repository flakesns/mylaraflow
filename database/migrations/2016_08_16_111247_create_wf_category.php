<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_name', 100)->unique();
            $table->text('category_desc');
            $table->text('lead_actionee');
            $table->text('approval');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wf_category');
    }
}
